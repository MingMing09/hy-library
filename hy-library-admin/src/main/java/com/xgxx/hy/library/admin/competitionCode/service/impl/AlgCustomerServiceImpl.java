package com.xgxx.hy.library.admin.competitionCode.service.impl;

import java.util.List;

import com.xgxx.hy.library.admin.competitionCode.mapper.AlgCustomerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xgxx.hy.library.admin.competitionCode.mapper.AlgCustomerMapper;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgCustomer;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgCustomerService;

/**
 * 客户Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-18
 */
@Service
public class AlgCustomerServiceImpl implements IAlgCustomerService
{
    @Autowired
    private AlgCustomerMapper algCustomerMapper;

    /**
     * 查询客户
     *
     * @param id 客户主键
     * @return 客户
     */
    @Override
    public AlgCustomer selectAlgCustomerById(Long id)
    {
        return algCustomerMapper.selectAlgCustomerById(id);
    }

    /**
     * 查询客户列表
     *
     * @param algCustomer 客户
     * @return 客户
     */
    @Override
    public List<AlgCustomer> selectAlgCustomerList(AlgCustomer algCustomer)
    {
        return algCustomerMapper.selectAlgCustomerList(algCustomer);
    }

    /**
     * 新增客户
     *
     * @param algCustomer 客户
     * @return 结果
     */
    @Override
    public int insertAlgCustomer(AlgCustomer algCustomer)
    {
        return algCustomerMapper.insertAlgCustomer(algCustomer);
    }

    /**
     * 修改客户
     *
     * @param algCustomer 客户
     * @return 结果
     */
    @Override
    public int updateAlgCustomer(AlgCustomer algCustomer)
    {
        return algCustomerMapper.updateAlgCustomer(algCustomer);
    }

    /**
     * 批量删除客户
     *
     * @param ids 需要删除的客户主键
     * @return 结果
     */
    @Override
    public int deleteAlgCustomerByIds(Long[] ids)
    {
        return algCustomerMapper.deleteAlgCustomerByIds(ids);
    }

    /**
     * 删除客户信息
     *
     * @param id 客户主键
     * @return 结果
     */
    @Override
    public int deleteAlgCustomerById(Long id)
    {
        return algCustomerMapper.deleteAlgCustomerById(id);
    }
}
