package com.xgxx.hy.library.admin.competitionCode.mapper;

import com.xgxx.hy.library.admin.competitionCode.domain.ProductionLine;

import java.util.List;

/**
 * 生产线Mapper接口
 *
 */
public interface ProductionLineMapper {
    /**
     * 查询图书
     *
     * @return 图书
     */
    public List<ProductionLine> selectProductionLineList(ProductionLine productionLine);


}
