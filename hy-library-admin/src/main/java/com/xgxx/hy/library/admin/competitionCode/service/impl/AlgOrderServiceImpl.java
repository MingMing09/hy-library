package com.xgxx.hy.library.admin.competitionCode.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.xgxx.hy.library.admin.common.utils.DateUtils;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgRate;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgRateService;
import com.xgxx.hy.library.admin.dingtalk.service.IDingtalkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xgxx.hy.library.admin.competitionCode.mapper.AlgOrderMapper;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgOrder;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgOrderService;

/**
 * 订单Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-18
 */
@Service
public class AlgOrderServiceImpl implements IAlgOrderService {
    @Autowired
    private AlgOrderMapper algOrderMapper;
    @Autowired
    private IAlgRateService rateService;
    @Autowired
    private IDingtalkService dingtalkService;

    /**
     * 查询订单
     *
     * @param id 订单主键
     * @return 订单
     */
    @Override
    public AlgOrder selectAlgOrderById(Long id) {
        return algOrderMapper.selectAlgOrderById(id);
    }

    /**
     * 查询订单列表
     *
     * @param algOrder 订单
     * @return 订单
     */
    @Override
    public List<AlgOrder> selectAlgOrderList(AlgOrder algOrder) {
        return algOrderMapper.selectAlgOrderList(algOrder);
    }

    /**
     * 新增订单
     *
     * @param algOrder 订单
     * @return 结果
     */
    @Override
    public int insertAlgOrder(AlgOrder algOrder) {
        if(null==algOrder.getIphone()||"".equals(algOrder.getIphone())){
            algOrder.setIphone("18252101046");
        }
        return algOrderMapper.insertAlgOrder(algOrder);
    }

    /**
     * 修改订单
     *
     * @param algOrder 订单
     * @return 结果
     */
    @Override
    public int updateAlgOrder(AlgOrder algOrder) {
        return algOrderMapper.updateAlgOrder(algOrder);
    }

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的订单主键
     * @return 结果
     */
    @Override
    public int deleteAlgOrderByIds(Long[] ids) {
        return algOrderMapper.deleteAlgOrderByIds(ids);
    }

    /**
     * 删除订单信息
     *
     * @param id 订单主键
     * @return 结果
     */
    @Override
    public int deleteAlgOrderById(Long id) {
        return algOrderMapper.deleteAlgOrderById(id);
    }

    @Override
    public int updateOrderRateByIds(Long[] ids) {
        for (int i = 0; i < ids.length; i++) {
            AlgOrder order = this.selectAlgOrderById(ids[i]);
            if (null != order) {
                Date d = new Date();
                // 要转换的格式
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
                // 格式化日期，日期->字符串
                String formatDate = sdf.format(d);
                dingtalkService.sendMsg(order.getIphone(), "尊敬的客户，您的订单排程完毕，即将投入生产，敬请期待！" + formatDate);
                AlgRate algRate = new AlgRate();
                algRate.setMaterialCode(order.getMaterialCode());
                algRate.setCustomerCode(order.getCustomerCode());
                List<AlgRate> algRates = rateService.selectAlgRateList(algRate);
                if (!algRates.isEmpty()) {
                    order.setRate(algRates.get(0).getRate());
                }
                this.updateAlgOrder(order);
            }
        }
        return 0;
    }
}
