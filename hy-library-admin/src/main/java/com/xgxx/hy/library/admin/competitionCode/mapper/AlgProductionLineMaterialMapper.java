package com.xgxx.hy.library.admin.competitionCode.mapper;

import java.util.List;

import com.xgxx.hy.library.admin.competitionCode.domain.AlgProductionLineMaterial;

/**
 * 产线物料配置Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-18
 */
public interface AlgProductionLineMaterialMapper 
{
    /**
     * 查询产线物料配置
     * 
     * @param id 产线物料配置主键
     * @return 产线物料配置
     */
    public AlgProductionLineMaterial selectAlgProductionLineMaterialById(Long id);

    /**
     * 查询产线物料配置列表
     * 
     * @param algProductionLineMaterial 产线物料配置
     * @return 产线物料配置集合
     */
    public List<AlgProductionLineMaterial> selectAlgProductionLineMaterialList(AlgProductionLineMaterial algProductionLineMaterial);

    /**
     * 新增产线物料配置
     * 
     * @param algProductionLineMaterial 产线物料配置
     * @return 结果
     */
    public int insertAlgProductionLineMaterial(AlgProductionLineMaterial algProductionLineMaterial);

    /**
     * 修改产线物料配置
     * 
     * @param algProductionLineMaterial 产线物料配置
     * @return 结果
     */
    public int updateAlgProductionLineMaterial(AlgProductionLineMaterial algProductionLineMaterial);

    /**
     * 删除产线物料配置
     * 
     * @param id 产线物料配置主键
     * @return 结果
     */
    public int deleteAlgProductionLineMaterialById(Long id);

    /**
     * 批量删除产线物料配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAlgProductionLineMaterialByIds(Long[] ids);
}
