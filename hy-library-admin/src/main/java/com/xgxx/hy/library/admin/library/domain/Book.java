package com.xgxx.hy.library.admin.library.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xgxx.hy.library.admin.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 图书对象 book
 *
 * @author zyc
 * @date 2021-10-20
 */
@Data
public class Book implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 图书编码
     */
    private String bookCode;

    /**
     * 图书名称
     */
    @Excel(name = "图书名称")
    private String bookName;

    /**
     * 国际标准图书编号
     */
    @Excel(name = "国际标准图书编号")
    private String isbn;

    /**
     * 图书中类
     */
    @Excel(name = "图书中类")
    private String bookType;

    /**
     * 出版社
     */
    @Excel(name = "出版社")
    private String bookPress;

    /**
     * 作者
     */
    @Excel(name = "作者")
    private String bookAuth;

    /**
     * 图书来源 0、工会采购 1、员工捐赠
     */
    @Excel(name = "图书来源 0、工会采购 1、员工捐赠")
    private Integer bookSource;

    @TableField(exist = false)
    private String bookSourceStr;

    /**
     * 图书位置 0、徐州 1、南京
     */
    @Excel(name = "图书位置 0、徐州 1、南京")
    private Integer bookLocation;

    @TableField(exist = false)
    private String bookLocationStr;
    /**
     * 图书简介
     */
    @Excel(name = "图书简介")
    private String bookRemark;

    /**
     * 图片信息
     */
    @Excel(name = "图片信息")
    private String bookPic;

    /**
     * 图书状态 0、在馆 1、借出 2、丢失
     */
    @Excel(name = "图书状态 0、在馆 1、借出 2、丢失")
    private Integer bookStatus;

    @TableField(exist = false)
    private String bookStatusStr;
    /**
     * 借阅次数
     */
    @Excel(name = "借阅次数")
    private Long bookBorrowCount;

    /**
     * 出版时间
     */
    @Excel(name = "出版时间")
    private String bookPublicationTime;

    /**
     * 价格
     */
    @Excel(name = "价格")
    private BigDecimal bookPrice;

    /**
     * 扫码信息
     */
    @TableField(exist = false)
    private String bookStr;


    /**
     * 借阅人，入参使用
     */
    @TableField(exist = false)
    private String borrowUser;
}
