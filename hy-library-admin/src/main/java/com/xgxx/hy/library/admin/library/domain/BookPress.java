package com.xgxx.hy.library.admin.library.domain;

import com.xgxx.hy.library.admin.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 出版社对象 book_press
 *
 * @author zyc
 * @date 2021-10-20
 */
@Data
public class BookPress implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 出版社名称
     */
    @Excel(name = "出版社名称")
    private String pressName;

    /**
     * 电话
     */
    @Excel(name = "电话")
    private String telephone;

    /**
     * 地址
     */
    @Excel(name = "地址")
    private String address;
}
