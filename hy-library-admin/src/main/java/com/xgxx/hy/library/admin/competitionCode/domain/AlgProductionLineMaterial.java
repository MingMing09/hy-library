package com.xgxx.hy.library.admin.competitionCode.domain;

import com.xgxx.hy.library.admin.common.annotation.Excel;
import com.xgxx.hy.library.admin.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 产线物料配置对象 alg_production_line_material
 *
 * @author ruoyi
 * @date 2022-10-18
 */
public class AlgProductionLineMaterial extends BaseEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    public AlgProductionLineMaterial(){

    }
    public AlgProductionLineMaterial(String productionLineCode,String materialCode,String capacity,String turnTimeStr){
        this();
        this.productionLineCode = productionLineCode;
        this.materialCode = materialCode;
        this.capacity = capacity;
        this.time = new BigDecimal(turnTimeStr);
    }
    /** 主键 */
    private Long id;

    /** 生产线编码 */
    @Excel(name = "生产线编码")
    private String productionLineCode;

    /** 物料编码 */
    @Excel(name = "物料编码")
    private String materialCode;

    /** 产能 */
    @Excel(name = "产能")
    private String capacity;

    /** 换料时间 */
    @Excel(name = "换料时间")
    private BigDecimal time;

    public BigDecimal getTime() {
        return time;
    }

    public void setTime(BigDecimal time) {
        this.time = time;
    }

    /** 状态 0、正常 1、暂停 */

    @Excel(name = "状态 0、正常 1、暂停")
    private Integer lineStatus;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setProductionLineCode(String productionLineCode)
    {
        this.productionLineCode = productionLineCode;
    }

    public String getProductionLineCode()
    {
        return productionLineCode;
    }
    public void setMaterialCode(String materialCode)
    {
        this.materialCode = materialCode;
    }

    public String getMaterialCode()
    {
        return materialCode;
    }
    public void setCapacity(String capacity)
    {
        this.capacity = capacity;
    }

    public String getCapacity()
    {
        return capacity;
    }

    public void setLineStatus(Integer lineStatus)
    {
        this.lineStatus = lineStatus;
    }

    public Integer getLineStatus()
    {
        return lineStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productionLineCode", getProductionLineCode())
            .append("materialCode", getMaterialCode())
            .append("capacity", getCapacity())
            .append("time", getTime())
            .append("lineStatus", getLineStatus())
            .toString();
    }
}
