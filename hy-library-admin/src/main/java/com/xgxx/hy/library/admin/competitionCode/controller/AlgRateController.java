package com.xgxx.hy.library.admin.competitionCode.controller;

import java.util.List;

import com.xgxx.hy.library.admin.common.annotation.Log;
import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import com.xgxx.hy.library.admin.common.enums.BusinessType;
import com.xgxx.hy.library.admin.common.utils.poi.ExcelUtil;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgRate;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgRateService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 实时占比Controller
 *
 * @author ruoyi
 * @date 2022-10-20
 */
@RestController
@RequestMapping("/admin/rate")
public class AlgRateController extends BaseController
{
    @Autowired
    private IAlgRateService algRateService;

    /**
     * 查询实时占比列表
     */
    @PreAuthorize("@ss.hasPermi('admin:rate:list')")
    @GetMapping("/list")
    public TableDataInfo list(AlgRate algRate)
    {
        startPage();
        List<AlgRate> list = algRateService.selectAlgRateList(algRate);
        return getDataTable(list);
    }

    /**
     * 导出实时占比列表
     */
    @PreAuthorize("@ss.hasPermi('admin:rate:export')")
    @Log(title = "实时占比", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AlgRate algRate)
    {
        List<AlgRate> list = algRateService.selectAlgRateList(algRate);
        ExcelUtil<AlgRate> util = new ExcelUtil<AlgRate>(AlgRate.class);
        return util.exportExcel(list, "实时占比数据");
    }

    /**
     * 获取实时占比详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:rate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(algRateService.selectAlgRateById(id));
    }

    /**
     * 新增实时占比
     */
    @PreAuthorize("@ss.hasPermi('admin:rate:add')")
    @Log(title = "实时占比", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AlgRate algRate)
    {
        return toAjax(algRateService.insertAlgRate(algRate));
    }

    /**
     * 修改实时占比
     */
    @PreAuthorize("@ss.hasPermi('admin:rate:edit')")
    @Log(title = "实时占比", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AlgRate algRate)
    {
        return toAjax(algRateService.updateAlgRate(algRate));
    }

    /**
     * 删除实时占比
     */
    @PreAuthorize("@ss.hasPermi('admin:rate:remove')")
    @Log(title = "实时占比", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(algRateService.deleteAlgRateByIds(ids));
    }
}
