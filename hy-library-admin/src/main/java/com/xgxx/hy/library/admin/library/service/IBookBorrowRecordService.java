package com.xgxx.hy.library.admin.library.service;

import com.xgxx.hy.library.admin.library.domain.BookBorrowRecord;

import java.util.Date;
import java.util.List;

/**
 * 图书借阅记录Service接口
 *
 * @author zyc
 * @date 2021-10-20
 */
public interface IBookBorrowRecordService {
    /**
     * 查询图书借阅记录
     *
     * @param id 图书借阅记录主键
     * @return 图书借阅记录
     */
    public BookBorrowRecord selectBookBorrowRecordById(Long id);

    /**
     * 查询图书借阅记录列表
     *
     * @param bookBorrowRecord 图书借阅记录
     * @return 图书借阅记录集合
     */
    public List<BookBorrowRecord> selectBookBorrowRecordList(BookBorrowRecord bookBorrowRecord);

    /**
     * 新增图书借阅记录
     *
     * @param bookBorrowRecord 图书借阅记录
     * @return 结果
     */
    public int insertBookBorrowRecord(BookBorrowRecord bookBorrowRecord);

    /**
     * 修改图书借阅记录
     *
     * @param bookBorrowRecord 图书借阅记录
     * @return 结果
     */
    public int updateBookBorrowRecord(BookBorrowRecord bookBorrowRecord);

    /**
     * 批量删除图书借阅记录
     *
     * @param ids 需要删除的图书借阅记录主键集合
     * @return 结果
     */
    public int deleteBookBorrowRecordByIds(Long[] ids);

    /**
     * 删除图书借阅记录信息
     *
     * @param id 图书借阅记录主键
     * @return 结果
     */
    public int deleteBookBorrowRecordById(Long id);

    /**
     * 添加借阅记录
     *
     * @param bookCode   图书编码
     * @param borrowUser 借书人
     * @param now        时间
     * @param code       操作类型
     */
    void addBookBorrowRecord(String bookCode, String borrowUser, Date now, int code);
}
