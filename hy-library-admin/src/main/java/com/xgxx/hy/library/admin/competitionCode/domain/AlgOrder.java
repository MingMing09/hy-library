package com.xgxx.hy.library.admin.competitionCode.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xgxx.hy.library.admin.common.annotation.Excel;
import com.xgxx.hy.library.admin.common.core.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 订单对象 alg_order
 *
 * @author ruoyi
 * @date 2022-10-18
 */
@Data
public class AlgOrder extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    public AlgOrder() {

    }

    public AlgOrder(String code, String customerCode, String materialCode, String qtyStr, String rateStr) {
        this();
        this.code = code;
        this.customerCode = customerCode;
        this.materialCode = materialCode;
        this.qty = new BigDecimal(qtyStr);
        this.rate = new BigDecimal(rateStr);
    }

    /**
     * 主键
     */
    private Long id;

    /**
     * 订单编码
     */
    @Excel(name = "订单编码")
    private String code;

    /**
     * 客户编码
     */
    @Excel(name = "客户编码")
    private String customerCode;

    /**
     * 物料编码
     */
    @Excel(name = "物料编码")
    private String materialCode;

    /**
     * 数量
     */
    @Excel(name = "数量")
    private BigDecimal qty;

    /**
     * 生产时间
     */
    @Excel(name = "生产时间")
    private BigDecimal time;

    /**
     * 权重
     */
    @Excel(name = "权重")
    private BigDecimal rate;

    /**
     * 状态 0、正常 1、暂停
     */
    @Excel(name = "状态 0、正常 1、暂停")
    private Integer lineStatus;

    /**
     * 完成数
     */
    @TableField(exist = false)
    private BigDecimal apsQty;

    private String iphone;
}
