package com.xgxx.hy.library.admin.competitionCode.controller;

import java.util.List;

import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import com.xgxx.hy.library.admin.common.enums.BusinessType;
import com.xgxx.hy.library.admin.common.utils.poi.ExcelUtil;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgOrder;
import com.xgxx.hy.library.admin.competitionCode.domain.ApsRequest;
import com.xgxx.hy.library.admin.competitionCode.domain.ApsResult;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgOrderService;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgRateService;
import com.xgxx.hy.library.admin.competitionCode.service.IApsService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xgxx.hy.library.admin.common.annotation.Log;

/**
 * 订单Controller
 *
 * @author ruoyi
 * @date 2022-10-18
 */
@RestController
@RequestMapping("/ahp/order")
public class AlgOrderController extends BaseController {
    @Autowired
    private IAlgOrderService algOrderService;
    @Autowired
    private IAlgRateService rateService;
    @Autowired
    private IApsService apsService;
    /**
     * 查询订单列表
     */
    @PreAuthorize("@ss.hasPermi('admin:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(AlgOrder algOrder) {
        startPage();
        List<AlgOrder> list = algOrderService.selectAlgOrderList(algOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @PreAuthorize("@ss.hasPermi('admin:order:export')")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AlgOrder algOrder) {
        List<AlgOrder> list = algOrderService.selectAlgOrderList(algOrder);
        ExcelUtil<AlgOrder> util = new ExcelUtil<AlgOrder>(AlgOrder.class);
        return util.exportExcel(list, "订单数据");
    }

    /**
     * 获取订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(algOrderService.selectAlgOrderById(id));
    }

    /**
     * 新增订单
     */
    @PreAuthorize("@ss.hasPermi('admin:order:add')")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AlgOrder algOrder) {
        return toAjax(algOrderService.insertAlgOrder(algOrder));
    }

    /**
     * 修改订单
     */
    @PreAuthorize("@ss.hasPermi('admin:order:edit')")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AlgOrder algOrder) {
        return toAjax(algOrderService.updateAlgOrder(algOrder));
    }

    /**
     * 删除订单
     */
    @PreAuthorize("@ss.hasPermi('admin:order:remove')")
    @Log(title = "订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(algOrderService.deleteAlgOrderByIds(ids));
    }

    @Log(title = "修改 订单所占权重", businessType = BusinessType.DELETE)
    @GetMapping(value = "/updateOrderRateByIds/{ids}")
    public AjaxResult updateOrderRateByIds(@PathVariable Long[] ids) {
        return toAjax(algOrderService.updateOrderRateByIds(ids));
    }

    @PostMapping(value = "/doAps")
    public AjaxResult doAps(@RequestBody ApsRequest apsRequest) {
        return apsService.doAps(apsRequest.getOrderList(),apsRequest.getStartTime(),apsRequest.getEndTime());
    }
}
