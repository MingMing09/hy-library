package com.xgxx.hy.library.admin.competitionCode.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zyc
 * @date 2022/10/20
 **/
@Data
public class ApsResult {
    private String productionLineCode;
    private String orderCode;
    private String materialCode;
    private Date startTime;
    private String startTimeStr;
    private Date endTime;
    private String endTimeStr;
    private boolean turnFlag;
    private BigDecimal num;
}
