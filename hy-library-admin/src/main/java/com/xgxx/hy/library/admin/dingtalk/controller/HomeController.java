package com.xgxx.hy.library.admin.dingtalk.controller;

import com.xgxx.hy.library.admin.common.config.DingTalkConfig;
import com.xgxx.hy.library.admin.common.constant.R;
import com.xgxx.hy.library.admin.common.utils.DateUtils;
import com.xgxx.hy.library.admin.dingtalk.entity.DingTalkUserDetail;
import com.xgxx.hy.library.admin.dingtalk.entity.DingTalkMsg;
import com.xgxx.hy.library.admin.dingtalk.service.IDingtalkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liwt
 * @version V2.4.0
 * @date 2021年10月22日 9:20
 * @Copyright:2021 徐工信息 All rights reserved.
 */
@RestController
@Slf4j
@RequestMapping("/dingtalk")
public class HomeController {

    @Autowired
    private DingTalkConfig dingTalkConfig;

    @Autowired
    private IDingtalkService dingtalkService;

    @RequestMapping("/config")
    public R<String> getConfig() {
        String agentId = this.dingTalkConfig.getAgentId();
        return R.data(agentId);
    }


    @RequestMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody DingTalkMsg msg) {
        R<String> ret = this.dingtalkService.sendMsg(msg.getMobile(), msg.getContent());
        return ret;
    }

    @RequestMapping("/user/detail/{mobile}")
    public R<DingTalkUserDetail> userDetail(@PathVariable String mobile) {
        R<DingTalkUserDetail> ret = this.dingtalkService.dingtalkUserDetail(mobile);
        return ret;
    }
    @RequestMapping("/test")
    public void test() {
        this.dingtalkService.sendMsg("18252101046", "You，您啊有书将逾期书本待归还，请及时处理；若已归还，请忽略本条信息！ 58.218.196.213，是公司外网地址。钉钉上，配置内网，无效！ ");
    }
}
