package com.xgxx.hy.library.admin.library.controller;

import com.xgxx.hy.library.admin.common.annotation.Log;
import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import com.xgxx.hy.library.admin.common.enums.BusinessType;
import com.xgxx.hy.library.admin.common.utils.poi.ExcelUtil;
import com.xgxx.hy.library.admin.library.domain.BookBorrowRecord;
import com.xgxx.hy.library.admin.library.service.IBookBorrowRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 图书借阅记录Controller
 *
 * @author zyc
 * @date 2021-10-20
 */
@RestController
@RequestMapping("/library/record")
public class BookBorrowRecordController extends BaseController {
    @Autowired
    private IBookBorrowRecordService bookBorrowRecordService;

    /**
     * 查询图书借阅记录列表
     */
    @GetMapping("/list")
    public TableDataInfo list(BookBorrowRecord bookBorrowRecord) {
        startPage();
        List<BookBorrowRecord> list = bookBorrowRecordService.selectBookBorrowRecordList(bookBorrowRecord);
        return getDataTable(list);
    }

    /**
     * 导出图书借阅记录列表
     */
    @GetMapping("/export")
    public AjaxResult export(BookBorrowRecord bookBorrowRecord) {
        List<BookBorrowRecord> list = bookBorrowRecordService.selectBookBorrowRecordList(bookBorrowRecord);
        ExcelUtil<BookBorrowRecord> util = new ExcelUtil<BookBorrowRecord>(BookBorrowRecord.class);
        return util.exportExcel(list, "图书借阅记录数据");
    }

    /**
     * 获取图书借阅记录详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(bookBorrowRecordService.selectBookBorrowRecordById(id));
    }

    /**
     * 新增图书借阅记录
     */
    @PostMapping
    public AjaxResult add(@RequestBody BookBorrowRecord bookBorrowRecord) {
        return toAjax(bookBorrowRecordService.insertBookBorrowRecord(bookBorrowRecord));
    }

    /**
     * 修改图书借阅记录
     */
    @PutMapping
    public AjaxResult edit(@RequestBody BookBorrowRecord bookBorrowRecord) {
        return toAjax(bookBorrowRecordService.updateBookBorrowRecord(bookBorrowRecord));
    }

    /**
     * 删除图书借阅记录
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(bookBorrowRecordService.deleteBookBorrowRecordByIds(ids));
    }
}
