package com.xgxx.hy.library.admin.library.controller;

import com.xgxx.hy.library.admin.common.annotation.Log;
import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import com.xgxx.hy.library.admin.common.enums.BusinessType;
import com.xgxx.hy.library.admin.common.utils.poi.ExcelUtil;
import com.xgxx.hy.library.admin.library.domain.BookType;
import com.xgxx.hy.library.admin.library.service.IBookTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 图书分类（中图分类）Controller
 *
 * @author zyc
 * @date 2021-10-20
 */
@RestController
@RequestMapping("/library/type")
public class BookTypeController extends BaseController {
    @Autowired
    private IBookTypeService bookTypeService;

    /**
     * 查询图书分类（中图分类）列表
     */
    @GetMapping("/list")
    public TableDataInfo list(BookType bookType) {
        startPage();
        List<BookType> list = bookTypeService.selectBookTypeList(bookType);
        return getDataTable(list);
    }

    /**
     * 导出图书分类（中图分类）列表
     */
    @GetMapping("/export")
    public AjaxResult export(BookType bookType) {
        List<BookType> list = bookTypeService.selectBookTypeList(bookType);
        ExcelUtil<BookType> util = new ExcelUtil<BookType>(BookType.class);
        return util.exportExcel(list, "图书分类（中图分类）数据");
    }

    /**
     * 获取图书分类（中图分类）详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(bookTypeService.selectBookTypeById(id));
    }

    /**
     * 新增图书分类（中图分类）
     */
    @PostMapping("add")
    public AjaxResult add(@RequestBody BookType bookType) {
        return bookTypeService.insertBookType(bookType);
    }

    /**
     * 修改图书分类（中图分类）
     */
    @PostMapping("edit")
    public AjaxResult edit(@RequestBody BookType bookType) {
        return bookTypeService.updateBookType(bookType);
    }

    /**
     * 删除图书分类（中图分类）
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(bookTypeService.deleteBookTypeByIds(ids));
    }


    /**
     * 删除图书分类（中图分类）
     */
    @PostMapping("/bookTypeTree")
    public AjaxResult bookTypeTree(@RequestBody BookType bookTypeEnter) {
        return AjaxResult.success(bookTypeService.bookTypeTree(bookTypeEnter));
    }
}
