package com.xgxx.hy.library.admin.library.mapper;

import com.xgxx.hy.library.admin.common.core.domain.entity.SysUser;
import com.xgxx.hy.library.admin.library.domain.Book;

import java.util.List;

/**
 * 图书Mapper接口
 *
 * @author zyc
 * @date 2021-10-20
 */
public interface BookMapper {
    /**
     * 查询图书
     *
     * @param id 图书主键
     * @return 图书
     */
    public Book selectBookById(Long id);

    /**
     * 查询图书列表
     *
     * @param book 图书
     * @return 图书集合
     */
    public List<Book> selectBookList(Book book);

    public List<Book> selectBookNameList(Book book);

    /**
     * 新增图书
     *
     * @param book 图书
     * @return 结果
     */
    public int insertBook(Book book);

    /**
     * 修改图书
     *
     * @param book 图书
     * @return 结果
     */
    public int updateBook(Book book);

    /**
     * 根据code修改
     *
     * @param book
     * @return
     */
    public int updateBookByCode(Book book);

    /**
     * 删除图书
     *
     * @param id 图书主键
     * @return 结果
     */
    public int deleteBookById(Long id);

    /**
     * 批量删除图书
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBookByIds(Long[] ids);

    /**
     * 根据code查询信息
     *
     * @param book
     * @return
     */
    public List<Book> selectBookListByCode(Book book);
    /**
     * 根据号码，获取详情信息
     * @param sysUser
     * @return
     */
    public List<SysUser> selectUserPhoneList(SysUser sysUser);

    /**
     * 借阅前十排行榜
     * @return
     */
    public  List<Book>  getUserPhang();

}
