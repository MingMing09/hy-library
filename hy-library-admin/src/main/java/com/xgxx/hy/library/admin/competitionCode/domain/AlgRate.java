package com.xgxx.hy.library.admin.competitionCode.domain;

import com.xgxx.hy.library.admin.common.annotation.Excel;
import com.xgxx.hy.library.admin.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 实时占比对象 alg_rate
 *
 * @author ruoyi
 * @date 2022-10-20
 */
public class AlgRate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 客户编码 */
    @Excel(name = "客户编码")
    private String customerCode;

    /** 物料编码 */
    @Excel(name = "物料编码")
    private String materialCode;

    /** 权重 */
    @Excel(name = "权重")
    private BigDecimal rate;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCustomerCode(String customerCode)
    {
        this.customerCode = customerCode;
    }

    public String getCustomerCode()
    {
        return customerCode;
    }
    public void setMaterialCode(String materialCode)
    {
        this.materialCode = materialCode;
    }

    public String getMaterialCode()
    {
        return materialCode;
    }
    public void setRate(BigDecimal rate)
    {
        this.rate = rate;
    }

    public BigDecimal getRate()
    {
        return rate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("customerCode", getCustomerCode())
            .append("materialCode", getMaterialCode())
            .append("rate", getRate())
            .toString();
    }
}
