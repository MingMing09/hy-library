package com.xgxx.hy.library.admin.library.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xgxx.hy.library.admin.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 图书分类（中图分类）对象 book_type
 *
 * @author zyc
 * @date 2021-10-20
 */
@Data
public class BookType implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 分类名称
     */
    @Excel(name = "分类名称")
    private String typeName;

    /**
     * 分类编码
     */
    @Excel(name = "分类编码")
    private String typeCode;

    /**
     * 上级分类
     */
    @Excel(name = "上级分类")
    private String parentCode;

    @TableField(exist = false)
    private List<BookType> children;


    public List<BookType> getChildren() {
       if(null==this.children){
           this.children = new ArrayList<>();
       }
        return children;
    }

    ;
}
