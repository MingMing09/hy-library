package com.xgxx.hy.library.admin.competitionCode.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xgxx.hy.library.admin.competitionCode.mapper.AlgMaterialMapper;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgMaterial;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgMaterialService;

/**
 * 物料Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-18
 */
@Service
public class AlgMaterialServiceImpl implements IAlgMaterialService
{
    @Autowired
    private AlgMaterialMapper algMaterialMapper;

    /**
     * 查询物料
     *
     * @param id 物料主键
     * @return 物料
     */
    @Override
    public AlgMaterial selectAlgMaterialById(Long id)
    {
        return algMaterialMapper.selectAlgMaterialById(id);
    }

    /**
     * 查询物料列表
     *
     * @param algMaterial 物料
     * @return 物料
     */
    @Override
    public List<AlgMaterial> selectAlgMaterialList(AlgMaterial algMaterial)
    {
        return algMaterialMapper.selectAlgMaterialList(algMaterial);
    }

    /**
     * 新增物料
     *
     * @param algMaterial 物料
     * @return 结果
     */
    @Override
    public int insertAlgMaterial(AlgMaterial algMaterial)
    {
        return algMaterialMapper.insertAlgMaterial(algMaterial);
    }

    /**
     * 修改物料
     *
     * @param algMaterial 物料
     * @return 结果
     */
    @Override
    public int updateAlgMaterial(AlgMaterial algMaterial)
    {
        return algMaterialMapper.updateAlgMaterial(algMaterial);
    }

    /**
     * 批量删除物料
     *
     * @param ids 需要删除的物料主键
     * @return 结果
     */
    @Override
    public int deleteAlgMaterialByIds(Long[] ids)
    {
        return algMaterialMapper.deleteAlgMaterialByIds(ids);
    }

    /**
     * 删除物料信息
     *
     * @param id 物料主键
     * @return 结果
     */
    @Override
    public int deleteAlgMaterialById(Long id)
    {
        return algMaterialMapper.deleteAlgMaterialById(id);
    }
}
