package com.xgxx.hy.library.admin.library.mapper;

import java.util.List;
import com.xgxx.hy.library.admin.library.domain.BookType;

/**
 * 图书分类（中图分类）Mapper接口
 * 
 * @author zyc
 * @date 2021-10-20
 */
public interface BookTypeMapper 
{
    /**
     * 查询图书分类（中图分类）
     * 
     * @param id 图书分类（中图分类）主键
     * @return 图书分类（中图分类）
     */
    public BookType selectBookTypeById(Long id);

    /**
     * 查询图书分类（中图分类）列表
     * 
     * @param bookType 图书分类（中图分类）
     * @return 图书分类（中图分类）集合
     */
    public List<BookType> selectBookTypeList(BookType bookType);

    public List<BookType> selectBookTypeNameList(BookType bookType);

    /**
     * 新增图书分类（中图分类）
     * 
     * @param bookType 图书分类（中图分类）
     * @return 结果
     */
    public int insertBookType(BookType bookType);

    /**
     * 修改图书分类（中图分类）
     * 
     * @param bookType 图书分类（中图分类）
     * @return 结果
     */
    public int updateBookType(BookType bookType);

    /**
     * 删除图书分类（中图分类）
     * 
     * @param id 图书分类（中图分类）主键
     * @return 结果
     */
    public int deleteBookTypeById(Long id);

    /**
     * 批量删除图书分类（中图分类）
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBookTypeByIds(Long[] ids);
}
