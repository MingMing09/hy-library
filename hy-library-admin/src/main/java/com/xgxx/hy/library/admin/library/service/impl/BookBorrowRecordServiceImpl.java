package com.xgxx.hy.library.admin.library.service.impl;

import com.xgxx.hy.library.admin.common.utils.DateUtils;
import com.xgxx.hy.library.admin.common.utils.spring.SpringUtils;
import com.xgxx.hy.library.admin.library.domain.BookBorrowRecord;
import com.xgxx.hy.library.admin.library.enums.BorrowStatusEnum;
import com.xgxx.hy.library.admin.library.mapper.BookBorrowRecordMapper;
import com.xgxx.hy.library.admin.library.service.IBookBorrowRecordService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 图书借阅记录Service业务层处理
 *
 * @author zyc
 * @date 2021-10-20
 */
@Service
@AllArgsConstructor
public class BookBorrowRecordServiceImpl implements IBookBorrowRecordService {

    private BookBorrowRecordMapper bookBorrowRecordMapper;

    /**
     * 查询图书借阅记录
     *
     * @param id 图书借阅记录主键
     * @return 图书借阅记录
     */
    @Override
    public BookBorrowRecord selectBookBorrowRecordById(Long id) {
        return bookBorrowRecordMapper.selectBookBorrowRecordById(id);
    }

    /**
     * 查询图书借阅记录列表
     *
     * @param bookBorrowRecord 图书借阅记录
     * @return 图书借阅记录
     */
    @Override
    public List<BookBorrowRecord> selectBookBorrowRecordList(BookBorrowRecord bookBorrowRecord) {
        List<BookBorrowRecord> list = bookBorrowRecordMapper.selectBookBorrowRecordList(bookBorrowRecord);
        list.forEach(e -> {
            e.setSysUser(SpringUtils.getBean(BookServiceImpl.class).getUserInfo(e.getBorrowUser()));
            e.setBook(SpringUtils.getBean(BookServiceImpl.class).selectBookByCode(e.getBookCode()));
        });
        return list;
    }

    /**
     * 新增图书借阅记录
     *
     * @param bookBorrowRecord 图书借阅记录
     * @return 结果
     */
    @Override
    public int insertBookBorrowRecord(BookBorrowRecord bookBorrowRecord) {
        return bookBorrowRecordMapper.insertBookBorrowRecord(bookBorrowRecord);
    }

    /**
     * 修改图书借阅记录
     *
     * @param bookBorrowRecord 图书借阅记录
     * @return 结果
     */
    @Override
    public int updateBookBorrowRecord(BookBorrowRecord bookBorrowRecord) {
        return bookBorrowRecordMapper.updateBookBorrowRecord(bookBorrowRecord);
    }

    /**
     * 批量删除图书借阅记录
     *
     * @param ids 需要删除的图书借阅记录主键
     * @return 结果
     */
    @Override
    public int deleteBookBorrowRecordByIds(Long[] ids) {
        return bookBorrowRecordMapper.deleteBookBorrowRecordByIds(ids);
    }

    /**
     * 删除图书借阅记录信息
     *
     * @param id 图书借阅记录主键
     * @return 结果
     */
    @Override
    public int deleteBookBorrowRecordById(Long id) {
        return bookBorrowRecordMapper.deleteBookBorrowRecordById(id);
    }

    @Override
    public void addBookBorrowRecord(String bookCode, String borrowUser, Date now, int code) {
        BookBorrowRecord bookBorrowRecord = new BookBorrowRecord();
        bookBorrowRecord.setBookCode(bookCode);
        bookBorrowRecord.setBorrowUser(borrowUser);
        bookBorrowRecord.setRecordTime(now);
        bookBorrowRecord.setRecordType(code);
        this.insertBookBorrowRecord(bookBorrowRecord);
    }
}
