package com.xgxx.hy.library.admin.competitionCode.mapper;

import java.util.List;

import com.xgxx.hy.library.admin.competitionCode.domain.AlgMaterial;

/**
 * 物料Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-18
 */
public interface AlgMaterialMapper 
{
    /**
     * 查询物料
     * 
     * @param id 物料主键
     * @return 物料
     */
    public AlgMaterial selectAlgMaterialById(Long id);

    /**
     * 查询物料列表
     * 
     * @param algMaterial 物料
     * @return 物料集合
     */
    public List<AlgMaterial> selectAlgMaterialList(AlgMaterial algMaterial);

    /**
     * 新增物料
     * 
     * @param algMaterial 物料
     * @return 结果
     */
    public int insertAlgMaterial(AlgMaterial algMaterial);

    /**
     * 修改物料
     * 
     * @param algMaterial 物料
     * @return 结果
     */
    public int updateAlgMaterial(AlgMaterial algMaterial);

    /**
     * 删除物料
     * 
     * @param id 物料主键
     * @return 结果
     */
    public int deleteAlgMaterialById(Long id);

    /**
     * 批量删除物料
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAlgMaterialByIds(Long[] ids);
}
