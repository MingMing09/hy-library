package com.xgxx.hy.library.admin.library.service.impl;

import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.library.domain.BookPress;
import com.xgxx.hy.library.admin.library.mapper.BookPressMapper;
import com.xgxx.hy.library.admin.library.service.IBookPressService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 出版社Service业务层处理
 *
 * @author zyc
 * @date 2021-10-20
 */
@Service
@AllArgsConstructor
public class BookPressServiceImpl implements IBookPressService {
    private BookPressMapper bookPressMapper;

    /**
     * 查询出版社
     *
     * @param id 出版社主键
     * @return 出版社
     */
    @Override
    public BookPress selectBookPressById(Long id) {
        return bookPressMapper.selectBookPressById(id);
    }

    /**
     * 查询出版社列表
     *
     * @param bookPress 出版社
     * @return 出版社
     */
    @Override
    public List<BookPress> selectBookPressList(BookPress bookPress) {
        return bookPressMapper.selectBookPressList(bookPress);
    }

    /**
     * 新增出版社
     *
     * @param bookPress 出版社
     * @return 结果
     */
    @Override
    public AjaxResult insertBookPress(BookPress bookPress) {
        BookPress bookPress1 = new BookPress();
        bookPress1.setPressName(bookPress.getPressName());
        List<BookPress> bookPresses = bookPressMapper.selectBookPressNameList(bookPress1);
        if (!bookPresses.isEmpty()) {
            return AjaxResult.error("编码重复！");
        }
        bookPressMapper.insertBookPress(bookPress);
        return AjaxResult.success("操作成功！");
    }

    /**
     * 修改出版社
     *
     * @param bookPress 出版社
     * @return 结果
     */
    @Override
    public AjaxResult updateBookPress(BookPress bookPress) {
        BookPress bookPress0 = bookPressMapper.selectBookPressById(bookPress.getId());
        //编码、名称 唯一
        BookPress bookPress1 = new BookPress();
        bookPress1.setPressName(bookPress.getPressName());
        List<BookPress> bookPresses = bookPressMapper.selectBookPressNameList(bookPress1);
        if (!bookPresses.isEmpty() && !bookPress.getPressName().equals(bookPress0.getPressName())) {
            return AjaxResult.error("名称重复！");
        }
        bookPressMapper.updateBookPress(bookPress);
        return AjaxResult.success("操作成功！");
    }

    /**
     * 批量删除出版社
     *
     * @param ids 需要删除的出版社主键
     * @return 结果
     */
    @Override
    public int deleteBookPressByIds(Long[] ids) {
        return bookPressMapper.deleteBookPressByIds(ids);
    }

    /**
     * 删除出版社信息
     *
     * @param id 出版社主键
     * @return 结果
     */
    @Override
    public int deleteBookPressById(Long id) {
        return bookPressMapper.deleteBookPressById(id);
    }
}
