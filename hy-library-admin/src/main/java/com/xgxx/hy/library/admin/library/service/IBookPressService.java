package com.xgxx.hy.library.admin.library.service;

import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.library.domain.BookPress;

import java.util.List;

/**
 * 出版社Service接口
 *
 * @author zyc
 * @date 2021-10-20
 */
public interface IBookPressService {
    /**
     * 查询出版社
     *
     * @param id 出版社主键
     * @return 出版社
     */
    public BookPress selectBookPressById(Long id);

    /**
     * 查询出版社列表
     *
     * @param bookPress 出版社
     * @return 出版社集合
     */
    public List<BookPress> selectBookPressList(BookPress bookPress);

    /**
     * 新增出版社
     *
     * @param bookPress 出版社
     * @return 结果
     */
    public AjaxResult insertBookPress(BookPress bookPress);

    /**
     * 修改出版社
     *
     * @param bookPress 出版社
     * @return 结果
     */
    public AjaxResult updateBookPress(BookPress bookPress);

    /**
     * 批量删除出版社
     *
     * @param ids 需要删除的出版社主键集合
     * @return 结果
     */
    public int deleteBookPressByIds(Long[] ids);

    /**
     * 删除出版社信息
     *
     * @param id 出版社主键
     * @return 结果
     */
    public int deleteBookPressById(Long id);
}
