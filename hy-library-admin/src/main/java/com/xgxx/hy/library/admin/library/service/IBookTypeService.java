package com.xgxx.hy.library.admin.library.service;

import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.library.domain.BookType;

import java.util.List;

/**
 * 图书分类（中图分类）Service接口
 *
 * @author zyc
 * @date 2021-10-20
 */
public interface IBookTypeService {
    /**
     * 查询图书分类（中图分类）
     *
     * @param id 图书分类（中图分类）主键
     * @return 图书分类（中图分类）
     */
    public BookType selectBookTypeById(Long id);

    /**
     * 查询图书分类（中图分类）列表
     *
     * @param bookType 图书分类（中图分类）
     * @return 图书分类（中图分类）集合
     */
    public List<BookType> selectBookTypeList(BookType bookType);

    /**
     * 新增图书分类（中图分类）
     *
     * @param bookType 图书分类（中图分类）
     * @return 结果
     */
    public AjaxResult insertBookType(BookType bookType);

    /**
     * 修改图书分类（中图分类）
     *
     * @param bookType 图书分类（中图分类）
     * @return 结果
     */
    public AjaxResult updateBookType(BookType bookType);

    /**
     * 批量删除图书分类（中图分类）
     *
     * @param ids 需要删除的图书分类（中图分类）主键集合
     * @return 结果
     */
    public int deleteBookTypeByIds(Long[] ids);

    /**
     * 删除图书分类（中图分类）信息
     *
     * @param id 图书分类（中图分类）主键
     * @return 结果
     */
    public int deleteBookTypeById(Long id);

    /**
     * 图书分类，树
     * @return
     */
    public List bookTypeTree(BookType bookTypeEnter);
}
