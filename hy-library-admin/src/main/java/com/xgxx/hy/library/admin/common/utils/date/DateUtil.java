package com.xgxx.hy.library.admin.common.utils.date;

/**
 * @author liwt
 * @version V2.4.0
 * @date 2021年10月11日 11:49
 * @Copyright:2021 徐工信息 All rights reserved.
 */
public class DateUtil extends cn.hutool.core.date.DateUtil {

    public static final String PATTERN_DATETIME = "yyyy-MM-dd HH:mm:ss";
    public static final String PATTERN_DATETIME_H = "yyyy-MM-dd HH:mm";
    public static final String PATTERN_DATE = "yyyy-MM-dd";
    public static final String PATTERN_TIME = "HH:mm:ss";

    public static final String SQL_PATTERN_DATETIME = "%Y-%m-%d %H:%i:%s";
    public static final String SQL_PATTERN_DATETIME_H = "%Y-%m-%d %H:%i";
    public static final String SQL_PATTERN_DATE = "%Y-%m-%d";
}
