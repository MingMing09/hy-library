package com.xgxx.hy.library.admin.competitionCode.controller;

import java.util.List;

import com.xgxx.hy.library.admin.common.annotation.Log;
import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import com.xgxx.hy.library.admin.common.enums.BusinessType;
import com.xgxx.hy.library.admin.common.utils.poi.ExcelUtil;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgMaterial;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgMaterialService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 物料Controller
 *
 * @author ruoyi
 * @date 2022-10-18
 */
@RestController
@RequestMapping("/ahp/material")
public class AlgMaterialController extends BaseController
{
    @Autowired
    private IAlgMaterialService algMaterialService;

    /**
     * 查询物料列表
     */
    @PreAuthorize("@ss.hasPermi('admin:material:list')")
    @GetMapping("/list")
    public TableDataInfo list(AlgMaterial algMaterial)
    {
        startPage();
        List<AlgMaterial> list = algMaterialService.selectAlgMaterialList(algMaterial);
        return getDataTable(list);
    }

    /**
     * 导出物料列表
     */
    @PreAuthorize("@ss.hasPermi('admin:material:export')")
    @Log(title = "物料", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AlgMaterial algMaterial)
    {
        List<AlgMaterial> list = algMaterialService.selectAlgMaterialList(algMaterial);
        ExcelUtil<AlgMaterial> util = new ExcelUtil<AlgMaterial>(AlgMaterial.class);
        return util.exportExcel(list, "物料数据");
    }

    /**
     * 获取物料详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:material:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(algMaterialService.selectAlgMaterialById(id));
    }

    /**
     * 新增物料
     */
    @PreAuthorize("@ss.hasPermi('admin:material:add')")
    @Log(title = "物料", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AlgMaterial algMaterial)
    {
        return toAjax(algMaterialService.insertAlgMaterial(algMaterial));
    }

    /**
     * 修改物料
     */
    @PreAuthorize("@ss.hasPermi('admin:material:edit')")
    @Log(title = "物料", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AlgMaterial algMaterial)
    {
        return toAjax(algMaterialService.updateAlgMaterial(algMaterial));
    }

    /**
     * 删除物料
     */
    @PreAuthorize("@ss.hasPermi('admin:material:remove')")
    @Log(title = "物料", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(algMaterialService.deleteAlgMaterialByIds(ids));
    }
}
