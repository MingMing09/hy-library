package com.xgxx.hy.library.admin.dingtalk.entity;

import lombok.Data;

import java.util.List;

/**
 * @author liwt
 * @version V2.4.0
 * @date 2021年10月22日 10:34
 * @Copyright:2021 徐工信息 All rights reserved.
 */
@Data
public class DingTalkUserDetail {
    private boolean boss;
    private String name;
    private String mobile;
    private String userid;
    private List<String> dept_id_list;
    private String deptName;
}
