package com.xgxx.hy.library.admin.competitionCode.service;

import com.xgxx.hy.library.admin.competitionCode.domain.ProductionLine;

import java.util.List;

/**
 * 生产线Service接口
 */
public interface IProductionLineService {
    /**
     * 查询生产线
     *
     * @return 生产线
     */
    List<ProductionLine> selectBookList(ProductionLine productionLine);

}
