package com.xgxx.hy.library.admin.library.enums;

public enum BorrowStatusEnum {
    BORROW(0, "在借"),
    BACK(1, "归还"),
    MISS(2, "丢失"),
    ;

    private String desc;
    private int code;

    BorrowStatusEnum(int code, String desc) {
        this.desc = desc;
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public int getCode() {
        return code;
    }

    public static String getValue(int code) {
        for (BorrowStatusEnum borrowRecordStatusEnum : BorrowStatusEnum.values()) {
            if (borrowRecordStatusEnum.getCode() == code) {
                return borrowRecordStatusEnum.getDesc();
            }
        }
        return "";
    }

    public static Integer getDesc(String desc) {
        for (BorrowStatusEnum borrowRecordStatusEnum : BorrowStatusEnum.values()) {
            if (borrowRecordStatusEnum.getDesc().equals(desc)) {
                return borrowRecordStatusEnum.getCode();
            }
        }
        return null;
    }
}
