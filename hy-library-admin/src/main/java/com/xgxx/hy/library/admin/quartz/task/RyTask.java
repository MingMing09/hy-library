package com.xgxx.hy.library.admin.quartz.task;

import com.xgxx.hy.library.admin.common.constant.R;
import com.xgxx.hy.library.admin.dingtalk.service.IDingtalkService;
import com.xgxx.hy.library.admin.library.service.IBookBorrowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xgxx.hy.library.admin.common.utils.StringUtils;

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask {
    @Autowired
    private IBookBorrowService bookBorrowService;
    @Autowired
    private IDingtalkService dingtalkService;

    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params) {
        System.out.println("执行有参方法：" + params);
        this.dingtalkService.sendMsg("18252101046", params);
    }

    public void ryNoParams() {
        System.out.println("执行无参方法");
        bookBorrowService.overdueAndBorrowAndBoticeList();
    }
}
