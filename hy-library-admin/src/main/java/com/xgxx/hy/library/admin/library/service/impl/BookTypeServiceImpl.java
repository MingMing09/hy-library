package com.xgxx.hy.library.admin.library.service.impl;

import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.library.domain.BookType;
import com.xgxx.hy.library.admin.library.mapper.BookTypeMapper;
import com.xgxx.hy.library.admin.library.service.IBookTypeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 图书分类（中图分类）Service业务层处理
 *
 * @author zyc
 * @date 2021-10-20
 */
@Service
@AllArgsConstructor
public class BookTypeServiceImpl implements IBookTypeService {
    private BookTypeMapper bookTypeMapper;

    /**
     * 查询图书分类（中图分类）
     *
     * @param id 图书分类（中图分类）主键
     * @return 图书分类（中图分类）
     */
    @Override
    public BookType selectBookTypeById(Long id) {
        return bookTypeMapper.selectBookTypeById(id);
    }

    /**
     * 查询图书分类（中图分类）列表
     *
     * @param bookType 图书分类（中图分类）
     * @return 图书分类（中图分类）
     */
    @Override
    public List<BookType> selectBookTypeList(BookType bookType) {
        return bookTypeMapper.selectBookTypeList(bookType);
    }

    /**
     * 新增图书分类（中图分类）
     *
     * @param bookType 图书分类（中图分类）
     * @return 结果
     */
    @Override
    public AjaxResult insertBookType(BookType bookType) {
        //编码、名称 唯一
        BookType bookType1 = new BookType();
        bookType1.setTypeCode(bookType.getTypeCode());
        List<BookType> bookTypes1 = bookTypeMapper.selectBookTypeNameList(bookType1);
        if (!bookTypes1.isEmpty()) {
            return AjaxResult.error("编码重复！");
        }
        BookType bookType2 = new BookType();
        bookType2.setTypeName(bookType.getTypeName());
        List<BookType> bookTypes2 = bookTypeMapper.selectBookTypeNameList(bookType2);
        if (!bookTypes2.isEmpty()) {
            return AjaxResult.error("名称重复！");
        }


        bookTypeMapper.insertBookType(bookType);
        return AjaxResult.success("操作成功！");
    }

    /**
     * 修改图书分类（中图分类）
     *
     * @param bookType 图书分类（中图分类）
     * @return 结果
     */
    @Override
    public AjaxResult updateBookType(BookType bookType) {
        //根据主键，查询原始数据
        BookType bookType0 = bookTypeMapper.selectBookTypeById(bookType.getId());
        //编码、名称 唯一
        BookType bookType1 = new BookType();
        bookType1.setTypeCode(bookType.getTypeCode());
        List<BookType> bookTypes1 = bookTypeMapper.selectBookTypeNameList(bookType1);
        if (!bookTypes1.isEmpty() && !bookType.getTypeCode().equals(bookType0.getTypeCode())) {
            return AjaxResult.error("编码重复！");
        }
        BookType bookType2 = new BookType();
        bookType2.setTypeName(bookType.getTypeName());
        List<BookType> bookTypes2 = bookTypeMapper.selectBookTypeNameList(bookType2);
        if (!bookTypes2.isEmpty() && !bookType.getTypeName().equals(bookType0.getTypeName())) {
            return AjaxResult.error("名称重复！");
        }
        bookTypeMapper.updateBookType(bookType);
        return AjaxResult.success("操作成功！");

    }

    /**
     * 批量删除图书分类（中图分类）
     *
     * @param ids 需要删除的图书分类（中图分类）主键
     * @return 结果
     */
    @Override
    public int deleteBookTypeByIds(Long[] ids) {
        return bookTypeMapper.deleteBookTypeByIds(ids);
    }

    /**
     * 删除图书分类（中图分类）信息
     *
     * @param id 图书分类（中图分类）主键
     * @return 结果
     */
    @Override
    public int deleteBookTypeById(Long id) {
        return bookTypeMapper.deleteBookTypeById(id);
    }

    @Override
    public List bookTypeTree(BookType bookTypeEnter) {

        /*
        循环的思路分析:
            查询所有，找到一级菜单，将一级菜单作为父菜单
            在根据数据库表中的id是pid的父级，找到每一个父级下面的子，然后将儿子装到父菜单下面
        */

        // 查询所有
        List<BookType> bookTypes = bookTypeMapper.selectBookTypeList(bookTypeEnter);
        // 表示父菜单
        List<BookType> listParent = new ArrayList<BookType>();
        // 这个map是为了让儿子通过id和pid的关系找到父级的
        Map<String, BookType> map = new HashMap<String, BookType>();
        for (BookType bookType : bookTypes) {
            map.put(bookType.getTypeCode(), bookType);
        }

        for (BookType bookType : bookTypes) {
            if (null != bookTypeEnter.getTypeCode() || null != bookTypeEnter.getTypeName()) {
                if (bookType.getTypeCode().equals(bookTypeEnter.getTypeCode())
                        || bookType.getTypeName().equals(bookTypeEnter.getTypeName())) {
                    listParent.add(bookType);
                } else {
                    // 儿子，还需要通过儿子去找到父亲
                    // 通过儿子的pid找到它父亲
                    BookType parent = map.get(bookType.getParentCode());
                    if (null != parent) {
                        parent.getChildren().add(bookType);
                    }
                }
            } else {
                // 最顶级的父菜单
                if (bookType.getParentCode().equals("0")) {
                    listParent.add(bookType);
                } else {
                    // 儿子，还需要通过儿子去找到父亲
                    // 通过儿子的pid找到它父亲
                    BookType parent = map.get(bookType.getParentCode());
                    if (null != parent) {
                        parent.getChildren().add(bookType);
                    }
                }
            }
        }
        return listParent;
    }
}
