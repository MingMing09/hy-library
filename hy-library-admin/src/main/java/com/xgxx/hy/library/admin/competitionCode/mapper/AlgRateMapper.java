package com.xgxx.hy.library.admin.competitionCode.mapper;

import java.util.List;

import com.xgxx.hy.library.admin.competitionCode.domain.AlgRate;

/**
 * 实时占比Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-20
 */
public interface AlgRateMapper 
{
    /**
     * 查询实时占比
     * 
     * @param id 实时占比主键
     * @return 实时占比
     */
    public AlgRate selectAlgRateById(Long id);

    /**
     * 查询实时占比列表
     * 
     * @param algRate 实时占比
     * @return 实时占比集合
     */
    public List<AlgRate> selectAlgRateList(AlgRate algRate);

    /**
     * 新增实时占比
     * 
     * @param algRate 实时占比
     * @return 结果
     */
    public int insertAlgRate(AlgRate algRate);

    /**
     * 修改实时占比
     * 
     * @param algRate 实时占比
     * @return 结果
     */
    public int updateAlgRate(AlgRate algRate);

    /**
     * 删除实时占比
     * 
     * @param id 实时占比主键
     * @return 结果
     */
    public int deleteAlgRateById(Long id);

    /**
     * 批量删除实时占比
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAlgRateByIds(Long[] ids);
}
