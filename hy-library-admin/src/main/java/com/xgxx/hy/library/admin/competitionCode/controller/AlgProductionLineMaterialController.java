package com.xgxx.hy.library.admin.competitionCode.controller;

import java.util.List;

import com.xgxx.hy.library.admin.common.annotation.Log;
import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import com.xgxx.hy.library.admin.common.enums.BusinessType;
import com.xgxx.hy.library.admin.common.utils.poi.ExcelUtil;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgProductionLineMaterial;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgProductionLineMaterialService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 产线物料配置Controller
 *
 * @author ruoyi
 * @date 2022-10-18
 */
@RestController
@RequestMapping("/ahp/productionLineMaterial")
public class AlgProductionLineMaterialController extends BaseController
{
    @Autowired
    private IAlgProductionLineMaterialService algProductionLineMaterialService;

    /**
     * 查询产线物料配置列表
     */
    @PreAuthorize("@ss.hasPermi('admin:material:list')")
    @GetMapping("/list")
    public TableDataInfo list(AlgProductionLineMaterial algProductionLineMaterial)
    {
        startPage();
        List<AlgProductionLineMaterial> list = algProductionLineMaterialService.selectAlgProductionLineMaterialList(algProductionLineMaterial);
        return getDataTable(list);
    }

    /**
     * 导出产线物料配置列表
     */
    @PreAuthorize("@ss.hasPermi('admin:material:export')")
    @Log(title = "产线物料配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AlgProductionLineMaterial algProductionLineMaterial)
    {
        List<AlgProductionLineMaterial> list = algProductionLineMaterialService.selectAlgProductionLineMaterialList(algProductionLineMaterial);
        ExcelUtil<AlgProductionLineMaterial> util = new ExcelUtil<AlgProductionLineMaterial>(AlgProductionLineMaterial.class);
        return util.exportExcel(list, "产线物料配置数据");
    }

    /**
     * 获取产线物料配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:material:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(algProductionLineMaterialService.selectAlgProductionLineMaterialById(id));
    }

    /**
     * 新增产线物料配置
     */
    @PreAuthorize("@ss.hasPermi('admin:material:add')")
    @Log(title = "产线物料配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AlgProductionLineMaterial algProductionLineMaterial)
    {
        return toAjax(algProductionLineMaterialService.insertAlgProductionLineMaterial(algProductionLineMaterial));
    }

    /**
     * 修改产线物料配置
     */
    @PreAuthorize("@ss.hasPermi('admin:material:edit')")
    @Log(title = "产线物料配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AlgProductionLineMaterial algProductionLineMaterial)
    {
        return toAjax(algProductionLineMaterialService.updateAlgProductionLineMaterial(algProductionLineMaterial));
    }

    /**
     * 删除产线物料配置
     */
    @PreAuthorize("@ss.hasPermi('admin:material:remove')")
    @Log(title = "产线物料配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(algProductionLineMaterialService.deleteAlgProductionLineMaterialByIds(ids));
    }
}
