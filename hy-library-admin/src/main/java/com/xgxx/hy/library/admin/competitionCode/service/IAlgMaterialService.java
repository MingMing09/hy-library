package com.xgxx.hy.library.admin.competitionCode.service;

import com.xgxx.hy.library.admin.competitionCode.domain.AlgMaterial;

import java.util.List;

/**
 * 物料Service接口
 * 
 * @author ruoyi
 * @date 2022-10-18
 */
public interface IAlgMaterialService 
{
    /**
     * 查询物料
     * 
     * @param id 物料主键
     * @return 物料
     */
    public AlgMaterial selectAlgMaterialById(Long id);

    /**
     * 查询物料列表
     * 
     * @param algMaterial 物料
     * @return 物料集合
     */
    public List<AlgMaterial> selectAlgMaterialList(AlgMaterial algMaterial);

    /**
     * 新增物料
     * 
     * @param algMaterial 物料
     * @return 结果
     */
    public int insertAlgMaterial(AlgMaterial algMaterial);

    /**
     * 修改物料
     * 
     * @param algMaterial 物料
     * @return 结果
     */
    public int updateAlgMaterial(AlgMaterial algMaterial);

    /**
     * 批量删除物料
     * 
     * @param ids 需要删除的物料主键集合
     * @return 结果
     */
    public int deleteAlgMaterialByIds(Long[] ids);

    /**
     * 删除物料信息
     * 
     * @param id 物料主键
     * @return 结果
     */
    public int deleteAlgMaterialById(Long id);
}
