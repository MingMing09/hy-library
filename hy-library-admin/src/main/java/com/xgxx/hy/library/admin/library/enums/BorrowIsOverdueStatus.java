package com.xgxx.hy.library.admin.library.enums;

public enum BorrowIsOverdueStatus {
    WYQ(0, "未逾期"),
    YYQ(1, "逾期"),;

    private Integer code; //编码
    private String desc; //描述

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static String getValue(Integer code) {
        for (BorrowIsOverdueStatus auditStatusCode : BorrowIsOverdueStatus.values()) {
            if (auditStatusCode.getCode().equals(code)) {
                return auditStatusCode.getDesc();
            }
        }

        return "";
    }

    BorrowIsOverdueStatus(int code, String desc) {
        this.code = (int)code;
        this.desc = desc;
    }
}