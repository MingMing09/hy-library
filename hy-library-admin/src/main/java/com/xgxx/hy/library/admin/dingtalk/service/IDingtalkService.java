package com.xgxx.hy.library.admin.dingtalk.service;

import com.xgxx.hy.library.admin.common.constant.R;
import com.xgxx.hy.library.admin.dingtalk.entity.DingTalkUserDetail;

public interface IDingtalkService {

    R<String> sendMsg(String userMobile,String msgContent);

    /**
     * 通过手机号获取钉钉用户信息
     * @param userMobile
     * @return
     */
    R<DingTalkUserDetail> dingtalkUserDetail(String userMobile);

    /**
     * 通过授权码获取当前登录人信息
     * @param requestAuthCode
     * @return
     */
    R<DingTalkUserDetail> login(String requestAuthCode);

}
