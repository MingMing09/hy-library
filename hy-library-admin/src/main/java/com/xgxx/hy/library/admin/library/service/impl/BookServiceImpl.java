package com.xgxx.hy.library.admin.library.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.domain.entity.SysUser;
import com.xgxx.hy.library.admin.library.domain.Book;
import com.xgxx.hy.library.admin.library.domain.BookBorrow;
import com.xgxx.hy.library.admin.library.enums.BookStatus;
import com.xgxx.hy.library.admin.library.mapper.BookMapper;
import com.xgxx.hy.library.admin.library.service.IBookBorrowService;
import com.xgxx.hy.library.admin.library.service.IBookService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 图书Service业务层处理
 *
 * @author zyc
 * @date 2021-10-20
 */
@Service
@AllArgsConstructor
public class BookServiceImpl implements IBookService {
    private BookMapper bookMapper;
    @Autowired
    private IBookBorrowService bookBorrowService;

    /**
     * 查询图书111
     *
     * @param id 图书主键
     * @return 图书
     */
    @Override
    public Book selectBookById(Long id) {
        return bookMapper.selectBookById(id);
    }

    /**
     * 查询图书列表
     *
     * @param book 图书
     * @return 图书
     */
    @Override
    public List<Book> selectBookList(Book book) {
        return bookMapper.selectBookList(book);
    }

    /**
     * 新增图书
     *
     * @param book 图书
     * @return 结果
     */
    @Override
    public AjaxResult insertBook(Book book) {
        if (null == book.getBookCode()) {
            return AjaxResult.error("编码不能为空！");
        }
        //编码唯一验证
        Book book1 = new Book();
        book1.setBookCode(book.getBookCode());
        List<Book> books = bookMapper.selectBookNameList(book1);
        if (!books.isEmpty()) {
            return AjaxResult.error("编码重复！");
        }
        bookMapper.insertBook(book);
        return AjaxResult.success("操作成功！");
    }

    /**
     * 修改图书
     *
     * @param book 图书
     * @return 结果
     */
    @Override
    public AjaxResult updateBook(Book book) {
        Book book0 = bookMapper.selectBookById(book.getId());
        //编码、名称 唯一
        Book book1 = new Book();
        book1.setBookCode(book.getBookCode());
        List<Book> books = bookMapper.selectBookNameList(book1);
        if (!books.isEmpty() && !book.getBookCode().equals(book0.getBookCode())) {
            return AjaxResult.error("编码重复！");
        }
        bookMapper.updateBook(book);
        return AjaxResult.success("操作成功！");
    }

    /**
     * 批量删除图书
     *
     * @param ids 需要删除的图书主键
     * @return 结果
     */
    @Override
    public int deleteBookByIds(Long[] ids) {
        return bookMapper.deleteBookByIds(ids);
    }

    /**
     * 删除图书信息
     *
     * @param id 图书主键
     * @return 结果
     */
    @Override
    public int deleteBookById(Long id) {
        return bookMapper.deleteBookById(id);
    }

    @Override
    public AjaxResult splitDataToInsert(String dataStr) {
        String[] result = dataStr.split("\\*");
        if (result.length != 9) {
            return AjaxResult.error("扫码错误！");
        }
        //判断是否编码重复
        Book bookCheck = new Book();
        bookCheck.setBookCode(result[1]);
        List<Book> books = bookMapper.selectBookList(bookCheck);
        if (!books.isEmpty()) {
            return AjaxResult.error("编码重复！");
        }
        //不重复，则入库
        Book book = new Book();
        book.setBookCode(result[1]);
        book.setBookName(result[2]);
        book.setIsbn(result[3]);
        book.setBookType(result[4]);
        book.setBookPress(result[5]);
        book.setBookAuth(result[6]);
        book.setBookRemark(result[7]);
        book.setBookPrice(BigDecimal.valueOf(Double.valueOf(result[8])));

        bookMapper.insertBook(book);
        return AjaxResult.success("操作成功！");
    }

    @Override
    public AjaxResult bookToBorrow(Book book) {
        //参数校验
        AjaxResult backResult = bookBorrowCheck(book);
        if (!backResult.isSuccess()) {
            return backResult;
        }
        //获取图书信息
        List<Book> books = bookMapper.selectBookListByCode(book);
        if (books.isEmpty()) {
            return AjaxResult.error("无相关图书！");
        }
        if(!books.get(0).getBookStatus().equals(BookStatus.ZG.getCode())){
            return AjaxResult.error("图书状态是"+BookStatus.getValue(books.get(0).getBookStatus())+",不能借阅！");
        }
        //借阅记录
        BookBorrow bookBorrow = new BookBorrow();
        bookBorrow.setBookCode(book.getBookCode());
        bookBorrow.setBorrowUser(book.getBorrowUser());
        AjaxResult result = bookBorrowService.bookBorrow(bookBorrow);
        if (!result.isSuccess()) {
            return result;
        }
        //借阅成功，修改图书状态
        book.setBookBorrowCount(((books.get(0).getBookBorrowCount() == null ? 0 : books.get(0).getBookBorrowCount()) + 1));
        book.setBookStatus(BookStatus.JC.getCode());
        bookMapper.updateBookByCode(book);
        return AjaxResult.success("操作成功！");
    }

    public AjaxResult bookBorrowCheck(Book book) {
        //参数校验
        if (null == book.getBookCode()) {
            return AjaxResult.error("图书编码不能为空！");
        }
        return AjaxResult.success("操作成功！");
    }

    @Override
    public AjaxResult bookAgainBorrow(Book book) {
        //参数校验
        AjaxResult backResult = bookBorrowCheck(book);
        if (!backResult.isSuccess()) {
            return backResult;
        }
        //获取图书信息
        List<Book> books = bookMapper.selectBookListByCode(book);
        if (books.isEmpty()) {
            return AjaxResult.error("无相关图书！");
        }
        //借阅记录
        BookBorrow bookBorrow = new BookBorrow();
        bookBorrow.setBookCode(book.getBookCode());
        AjaxResult result = bookBorrowService.bookBorrowAgain(bookBorrow);
        if (!result.isSuccess()) {
            return result;
        }
        //借阅成功，修改图书次数
        book.setBookBorrowCount(((books.get(0).getBookBorrowCount() == null ? 0 : books.get(0).getBookBorrowCount()) + 1));
        book.setBookStatus(BookStatus.JC.getCode());
        bookMapper.updateBookByCode(book);
        return AjaxResult.success("操作成功！");
    }

    @Override
    public AjaxResult bookBackBorrow(Book book) {
        //参数校验
        AjaxResult backResult = bookBorrowCheck(book);
        if (!backResult.isSuccess()) {
            return backResult;
        }
        //获取图书信息
        List<Book> books = bookMapper.selectBookListByCode(book);
        if (books.isEmpty()) {
            return AjaxResult.error("无相关图书！");
        }
        //借阅记录
        BookBorrow bookBorrow = new BookBorrow();
        bookBorrow.setBookCode(book.getBookCode());
        AjaxResult result = bookBorrowService.bookBorrowBack(bookBorrow);
        if (!result.isSuccess()) {
            return result;
        }
        //借阅成功，修改图书状态为“在馆”
        book.setBookStatus(BookStatus.ZG.getCode());
        bookMapper.updateBookByCode(book);
        return AjaxResult.success("操作成功！");
    }

    @Override
    public AjaxResult bookLoseBorrow(Book book) {
        //参数校验
        AjaxResult backResult = bookBorrowCheck(book);
        if (!backResult.isSuccess()) {
            return backResult;
        }
        //获取图书信息
        List<Book> books = bookMapper.selectBookListByCode(book);
        if (books.isEmpty()) {
            return AjaxResult.error("无相关图书！");
        }
        //借阅记录
        BookBorrow bookBorrow = new BookBorrow();
        bookBorrow.setBookCode(book.getBookCode());
        AjaxResult result = bookBorrowService.bookMiss(bookBorrow);
        if (!result.isSuccess()) {
            return result;
        }
        //借阅成功，修改图书状态为“丢失”
        book.setBookStatus(BookStatus.DS.getCode());
        bookMapper.updateBookByCode(book);
        return AjaxResult.success("操作成功！");
    }

    @Override
    public AjaxResult bookLoseBorrowPc(Book book) {
        //参数校验
        AjaxResult backResult = bookBorrowCheck(book);
        if (!backResult.isSuccess()) {
            return backResult;
        }
        //获取图书信息
        List<Book> books = bookMapper.selectBookListByCode(book);
        if (books.isEmpty()) {
            return AjaxResult.error("无相关图书！");
        }
        //借阅成功，修改图书状态为“丢失”
        book.setBookStatus(BookStatus.DS.getCode());
        bookMapper.updateBookByCode(book);
        return AjaxResult.success("操作成功！");
    }

    @Override
    public Book selectBookByCode(String bookCode) {
        Book book = new Book();
        book.setBookCode(bookCode);
        List<Book> books = bookMapper.selectBookListByCode(book);
        return CollectionUtil.isNotEmpty(books) ? books.get(0) : null;
    }

    @Override
    public AjaxResult getUserByPhone(SysUser user) {
        List<SysUser> sysUsers = bookMapper.selectUserPhoneList(user);
        if (!sysUsers.isEmpty()) {
            return AjaxResult.success(sysUsers.get(0));
        }
        return AjaxResult.error("无相关用户！");
    }

    @Override
    public AjaxResult getUserPhang() {
        List<Book> books = bookMapper.getUserPhang();
        return AjaxResult.success(books);
    }

    @Override
    public SysUser getUserInfo(String borrowUser) {
        SysUser sysUser = new SysUser();
        sysUser.setPhonenumber(borrowUser);
        List<SysUser> sysUsers = bookMapper.selectUserPhoneList(sysUser);
        if (!sysUsers.isEmpty()) {
            return sysUsers.get(0);
        }
        return null;
    }
}
