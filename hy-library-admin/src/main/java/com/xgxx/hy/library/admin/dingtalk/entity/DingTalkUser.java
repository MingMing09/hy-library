package com.xgxx.hy.library.admin.dingtalk.entity;

import lombok.Data;

/**
 * @author liwt
 * @version V2.4.0
 * @date 2021年10月22日 10:34
 * @Copyright:2021 徐工信息 All rights reserved.
 */
@Data
public class DingTalkUser {
    private String userid;
}
