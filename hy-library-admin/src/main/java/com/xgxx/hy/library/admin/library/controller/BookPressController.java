package com.xgxx.hy.library.admin.library.controller;

import com.xgxx.hy.library.admin.common.annotation.Log;
import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import com.xgxx.hy.library.admin.common.enums.BusinessType;
import com.xgxx.hy.library.admin.common.utils.poi.ExcelUtil;
import com.xgxx.hy.library.admin.library.domain.BookPress;
import com.xgxx.hy.library.admin.library.service.IBookPressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 出版社Controller
 *
 * @author zyc
 * @date 2021-10-20
 */
@RestController
@RequestMapping("/library/press")
public class BookPressController extends BaseController {
    @Autowired
    private IBookPressService bookPressService;

    /**
     * 查询出版社列表
     */
    @GetMapping("/list")
    public TableDataInfo list(BookPress bookPress) {
        startPage();
        List<BookPress> list = bookPressService.selectBookPressList(bookPress);
        return getDataTable(list);
    }

    /**
     * 导出出版社列表
     */
    @Log(title = "出版社", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BookPress bookPress) {
        List<BookPress> list = bookPressService.selectBookPressList(bookPress);
        ExcelUtil<BookPress> util = new ExcelUtil<BookPress>(BookPress.class);
        return util.exportExcel(list, "出版社数据");
    }

    /**
     * 获取出版社详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(bookPressService.selectBookPressById(id));
    }

    /**
     * 新增出版社
     */
    @PostMapping("add")
    public AjaxResult add(@RequestBody BookPress bookPress) {
        return bookPressService.insertBookPress(bookPress);
    }

    /**
     * 修改出版社
     */
    @PostMapping("edit")
    public AjaxResult edit(@RequestBody BookPress bookPress) {
        return bookPressService.updateBookPress(bookPress);
    }

    /**
     * 删除出版社
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(bookPressService.deleteBookPressByIds(ids));
    }
}
