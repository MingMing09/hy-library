package com.xgxx.hy.library.admin.library.mapper;

import com.xgxx.hy.library.admin.library.domain.BookBorrow;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 图书借阅Mapper接口
 *
 * @author zyc
 * @date 2021-10-20
 */
public interface BookBorrowMapper {
    /**
     * 查询图书借阅
     *
     * @param id 图书借阅主键
     * @return 图书借阅
     */
    public BookBorrow selectBookBorrowById(Long id);

    /**
     * 查询图书借阅列表
     *
     * @param bookBorrow 图书借阅
     * @return 图书借阅集合
     */
    public List<BookBorrow> selectBookBorrowList(BookBorrow bookBorrow);

    /**
     * 新增图书借阅
     *
     * @param bookBorrow 图书借阅
     * @return 结果
     */
    public int insertBookBorrow(BookBorrow bookBorrow);

    /**
     * 修改图书借阅
     *
     * @param bookBorrow 图书借阅
     * @return 结果
     */
    public int updateBookBorrow(BookBorrow bookBorrow);

    /**
     * 删除图书借阅
     *
     * @param id 图书借阅主键
     * @return 结果
     */
    public int deleteBookBorrowById(Long id);

    /**
     * 批量删除图书借阅
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBookBorrowByIds(Long[] ids);

    /**
     * 查询图书借阅
     *
     * @param bookBorrow   借书记录
     * @return 图书借阅对象
     */
    public BookBorrow selectBookBorrowByUser(BookBorrow bookBorrow);
}
