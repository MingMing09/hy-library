package com.xgxx.hy.library.admin.library.controller;

import com.xgxx.hy.library.admin.common.annotation.Log;
import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.domain.entity.SysUser;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import com.xgxx.hy.library.admin.common.enums.BusinessType;
import com.xgxx.hy.library.admin.common.utils.poi.ExcelUtil;
import com.xgxx.hy.library.admin.library.domain.Book;
import com.xgxx.hy.library.admin.library.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 图书Controller
 *
 * @author zyc
 * @date 2021-10-20
 */
@RestController
@RequestMapping("/library/book")
public class BookController extends BaseController {
    @Autowired
    private IBookService bookService;

    /**
     * 查询图书列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Book book) {
        startPage();
        List<Book> list = bookService.selectBookList(book);
        return getDataTable(list);
    }

    /**
     * 导出图书列表
     */
    @GetMapping("/export")
    public AjaxResult export(Book book) {
        List<Book> list = bookService.selectBookList(book);
        ExcelUtil<Book> util = new ExcelUtil<Book>(Book.class);
        return util.exportExcel(list, "图书数据");
    }

    /**
     * 获取图书详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(bookService.selectBookById(id));
    }

    /**
     * 新增图书
     */
    @PostMapping("add")
    public AjaxResult add(@RequestBody Book book) {
        return bookService.insertBook(book);
    }

    /**
     * 修改图书
     */
    @PostMapping("edit")
    public AjaxResult edit(@RequestBody Book book) {
        return bookService.updateBook(book);
    }

    /**
     * 删除图书
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(bookService.deleteBookByIds(ids));
    }
//=============================================================================================================================

    /**
     * 扫码，入库
     */
    @PostMapping(value = "/splitDataToInsert")
    public AjaxResult splitDataToInsert(@RequestBody Book book) {
        return bookService.splitDataToInsert(book.getBookStr());
    }

    /**
     * 图书借阅（借阅入口）
     */
    @PostMapping(value = "/bookToBorrow")
    public AjaxResult bookToBorrow(@RequestBody Book book) {
        return bookService.bookToBorrow(book);
    }

    /**
     * 图书续借（续借入口）
     */
    @PostMapping(value = "/bookAgainBorrow")
    public AjaxResult bookAgainBorrow(@RequestBody Book book) {
        return bookService.bookAgainBorrow(book);
    }

    /**
     * 图书归还（归还入口）
     */
    @PostMapping(value = "/bookBackBorrow")
    public AjaxResult bookBackBorrow(@RequestBody Book book) {
        return bookService.bookBackBorrow(book);
    }

    /**
     * 图书挂失（挂失入口）
     */
    @PostMapping(value = "/bookLoseBorrow")
    public AjaxResult bookLoseBorrow(@RequestBody Book book) {
        return bookService.bookLoseBorrow(book);
    }

    /**
     * 图书挂失（挂失入口，不关联记录）PC端
     */
    @PostMapping(value = "/bookLoseBorrowPc")
    public AjaxResult bookLoseBorrowPc(@RequestBody Book book) {
        return bookService.bookLoseBorrowPc(book);
    }

    /**
     * 据图书编码获取图书信息
     */
    @PostMapping(value = "/selectBookByCode")
    public AjaxResult selectBookByCode(@RequestBody Book book) {
        book = bookService.selectBookByCode(book.getBookCode());
        return AjaxResult.success(book);
    }

    /**
     * 根据号码，获取详情信息
     */
    @PostMapping("/getUserByPhone")
    public AjaxResult getUserByPhone(@RequestBody SysUser user) {
        return bookService.getUserByPhone(user);
    }

    /**
     * 前十排行榜
     */
    @GetMapping(value = "/getUserPhang")
    public AjaxResult getUserPhang() {
        return bookService.getUserPhang();
    }
}
