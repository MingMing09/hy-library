package com.xgxx.hy.library.admin.competitionCode.domain;

import com.xgxx.hy.library.admin.common.annotation.Excel;
import com.xgxx.hy.library.admin.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.io.Serializable;

/**
 * 物料对象 alg_material
 *
 * @author ruoyi
 * @date 2022-10-18
 */
public class AlgMaterial extends BaseEntity implements Serializable

        {
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 物料编码 */
    @Excel(name = "物料编码")
    private String code;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String name;

    /** 状态 0、正常 1、暂停 */
    @Excel(name = "状态 0、正常 1、暂停")
    private Integer lineStatus;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setLineStatus(Integer lineStatus)
    {
        this.lineStatus = lineStatus;
    }

    public Integer getLineStatus()
    {
        return lineStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("lineStatus", getLineStatus())
            .toString();
    }
}
