package com.xgxx.hy.library.admin.competitionCode.domain;

import com.xgxx.hy.library.admin.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Ahp 计算因子
 */
@Data
public class AhpCaseA implements Serializable {
    private static final long serialVersionUID = 1L;


    private BigDecimal case1;
    private BigDecimal case2;
    private BigDecimal case3;
    private BigDecimal case4;
    private BigDecimal case5;
    private BigDecimal case6;
    private BigDecimal case7;
    private BigDecimal case8;

    public void setCase(int xx, BigDecimal uu) {
        if (xx == 1) {
            this.case1 = uu;
        }
        if (xx == 2) {
            this.case2 = uu;
        }
        if (xx == 3) {
            this.case3 = uu;
        }
        if (xx == 4) {
            this.case4 = uu;
        }
        if (xx == 5) {
            this.case5 = uu;
        }
        if (xx == 6) {
            this.case6 = uu;
        }
        if (xx == 7) {
            this.case7 = uu;
        }
        if (xx == 8) {
            this.case8 = uu;
        }
    }

    public BigDecimal getCase(int xx) {
        BigDecimal yy = BigDecimal.ZERO;
        if (xx == 1) {
            return case1;
        }
        if (xx == 2) {
            return case2;
        }
        if (xx == 3) {
            return case3;
        }
        if (xx == 4) {
            return case4;
        }
        if (xx == 5) {
            return case5;
        }
        if (xx == 6) {
            return case6;
        }
        if (xx == 7) {
            return case7;
        }
        if (xx == 8) {
            return case8;
        }
        return yy;
    }


}
