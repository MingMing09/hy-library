package com.xgxx.hy.library.admin.competitionCode.service;

import com.xgxx.hy.library.admin.competitionCode.domain.AlgOrder;

import java.util.List;

/**
 * 订单Service接口
 * 
 * @author ruoyi
 * @date 2022-10-18
 */
public interface IAlgOrderService 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public AlgOrder selectAlgOrderById(Long id);

    /**
     * 查询订单列表
     * 
     * @param algOrder 订单
     * @return 订单集合
     */
    public List<AlgOrder> selectAlgOrderList(AlgOrder algOrder);

    /**
     * 新增订单
     * 
     * @param algOrder 订单
     * @return 结果
     */
    public int insertAlgOrder(AlgOrder algOrder);

    /**
     * 修改订单
     * 
     * @param algOrder 订单
     * @return 结果
     */
    public int updateAlgOrder(AlgOrder algOrder);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的订单主键集合
     * @return 结果
     */
    public int deleteAlgOrderByIds(Long[] ids);

    /**
     * 删除订单信息
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteAlgOrderById(Long id);

    /**
     * 修改 订单所占权重
     * @param ids
     * @return
     */
    public int updateOrderRateByIds(Long[] ids);
}
