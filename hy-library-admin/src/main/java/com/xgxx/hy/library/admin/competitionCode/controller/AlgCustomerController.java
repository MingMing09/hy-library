package com.xgxx.hy.library.admin.competitionCode.controller;

import java.util.List;

import com.xgxx.hy.library.admin.common.annotation.Log;
import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import com.xgxx.hy.library.admin.common.enums.BusinessType;
import com.xgxx.hy.library.admin.common.utils.poi.ExcelUtil;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgCustomer;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgCustomerService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 客户Controller
 *
 * @author ruoyi
 * @date 2022-10-18
 */
@RestController
@RequestMapping("/ahp/customer")
public class AlgCustomerController extends BaseController
{
    @Autowired
    private IAlgCustomerService algCustomerService;

    /**
     * 查询客户列表
     */
    @PreAuthorize("@ss.hasPermi('admin:customer:list')")
    @GetMapping("/list")
    public TableDataInfo list(AlgCustomer algCustomer)
    {
        startPage();
        List<AlgCustomer> list = algCustomerService.selectAlgCustomerList(algCustomer);
        return getDataTable(list);
    }

    /**
     * 导出客户列表
     */
    @PreAuthorize("@ss.hasPermi('admin:customer:export')")
    @Log(title = "客户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AlgCustomer algCustomer)
    {
        List<AlgCustomer> list = algCustomerService.selectAlgCustomerList(algCustomer);
        ExcelUtil<AlgCustomer> util = new ExcelUtil<AlgCustomer>(AlgCustomer.class);
        return util.exportExcel(list, "客户数据");
    }

    /**
     * 获取客户详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:customer:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(algCustomerService.selectAlgCustomerById(id));
    }

    /**
     * 新增客户
     */
    @PreAuthorize("@ss.hasPermi('admin:customer:add')")
    @Log(title = "客户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AlgCustomer algCustomer)
    {
        return toAjax(algCustomerService.insertAlgCustomer(algCustomer));
    }

    /**
     * 修改客户
     */
    @PreAuthorize("@ss.hasPermi('admin:customer:edit')")
    @Log(title = "客户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AlgCustomer algCustomer)
    {
        return toAjax(algCustomerService.updateAlgCustomer(algCustomer));
    }

    /**
     * 删除客户
     */
    @PreAuthorize("@ss.hasPermi('admin:customer:remove')")
    @Log(title = "客户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(algCustomerService.deleteAlgCustomerByIds(ids));
    }
}
