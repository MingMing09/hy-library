package com.xgxx.hy.library.admin.common.constant;

/**
 * @author liwt
 * @version V2.4.0
 * @date 2021年09月11日 12:48
 * @Copyright:2021 徐工信息 All rights reserved.
 */
public class Constant {
    //webflux controller mapping path
    public static final String AUTH_FAILED_PATH = "/auth/failed";
    public static final String BACKEND_EXCEPTION_PATH = "/backend/exception";

    public static final String WEB_FILTER_ATTR_NAME = "filterchain";
    //认证失败属性名
    public static final String AUTH_ERROR_ATTR_NAME = "auth-error";
    //后端服务异常属性名
    public static final String BACKEND_EXCEPTION_ATTR_NAME = "backend-exception";
}
