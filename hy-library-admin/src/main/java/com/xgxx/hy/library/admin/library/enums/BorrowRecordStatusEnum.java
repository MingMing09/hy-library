package com.xgxx.hy.library.admin.library.enums;

public enum BorrowRecordStatusEnum {
    BORROW(0, "借阅"),
    AGAIN(1, "续借"),
    BACK(2, "归还"),
    MISS(3, "丢失"),
    MISS_BACK(4, "丢失归还"),
    ;

    private String desc;
    private int code;

    BorrowRecordStatusEnum(int code, String desc) {
        this.desc = desc;
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public int getCode() {
        return code;
    }

    public static String getValue(int code) {
        for (BorrowRecordStatusEnum borrowRecordStatusEnum : BorrowRecordStatusEnum.values()) {
            if (borrowRecordStatusEnum.getCode() == code) {
                return borrowRecordStatusEnum.getDesc();
            }
        }
        return "";
    }

    public static Integer getDesc(String desc) {
        for (BorrowRecordStatusEnum borrowRecordStatusEnum : BorrowRecordStatusEnum.values()) {
            if (borrowRecordStatusEnum.getDesc().equals(desc)) {
                return borrowRecordStatusEnum.getCode();
            }
        }
        return null;
    }
}
