package com.xgxx.hy.library.admin.dingtalk.controller;

import com.xgxx.hy.library.admin.common.constant.R;
import com.xgxx.hy.library.admin.dingtalk.entity.DingTalkUserDetail;
import com.xgxx.hy.library.admin.dingtalk.service.IDingtalkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liwt
 * @version V2.4.0
 * @date 2021年10月22日 11:50
 * @Copyright:2021 徐工信息 All rights reserved.
 */
@RestController
@RequestMapping("/dingtalk")
@Slf4j
public class LoginController {


    @Autowired
    private IDingtalkService dingtalkService;

    /**
     * 登录方法
     *
     * @param requestAuthCode 登录信息
     * @return 结果
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public R<DingTalkUserDetail> login(@RequestParam("code") String requestAuthCode) {
        R<DingTalkUserDetail> ret = this.dingtalkService.login(requestAuthCode);
        log.error(ret.getData().getMobile()+"<-----");
        return ret;
    }


}
