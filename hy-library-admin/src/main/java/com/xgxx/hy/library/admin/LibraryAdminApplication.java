package com.xgxx.hy.library.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author ruoyi
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@MapperScan("com.xgxx.hy.library.admin.**.mapper")
public class LibraryAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(LibraryAdminApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  汉云图书馆管理系统 启动成功 ");
    }
}
