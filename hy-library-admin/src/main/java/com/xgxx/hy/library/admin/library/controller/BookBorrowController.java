package com.xgxx.hy.library.admin.library.controller;

import com.xgxx.hy.library.admin.common.annotation.Log;
import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import com.xgxx.hy.library.admin.common.enums.BusinessType;
import com.xgxx.hy.library.admin.common.utils.poi.ExcelUtil;
import com.xgxx.hy.library.admin.library.domain.BookBorrow;
import com.xgxx.hy.library.admin.library.enums.BorrowStatusEnum;
import com.xgxx.hy.library.admin.library.service.IBookBorrowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 图书借阅Controller
 *
 * @author zyc
 * @date 2021-10-20
 */
@RestController
@RequestMapping("/library/borrow")
public class BookBorrowController extends BaseController {
    @Autowired
    private IBookBorrowService bookBorrowService;

    /**
     * 查询图书借阅列表
     */
    @GetMapping("/list")
    public TableDataInfo list(BookBorrow bookBorrow) {
        startPage();
        List<BookBorrow> list = bookBorrowService.selectBookBorrowList(bookBorrow);
        return getDataTable(list);
    }

    /**
     * 导出图书借阅列表
     */
    @GetMapping("/export")
    public AjaxResult export(BookBorrow bookBorrow) {
        List<BookBorrow> list = bookBorrowService.selectBookBorrowList(bookBorrow);
        ExcelUtil<BookBorrow> util = new ExcelUtil<BookBorrow>(BookBorrow.class);
        return util.exportExcel(list, "图书借阅数据");
    }

    /**
     * 获取图书借阅详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(bookBorrowService.selectBookBorrowById(id));
    }

    /**
     * 新增图书借阅
     */
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BookBorrow bookBorrow) {
        return toAjax(bookBorrowService.insertBookBorrow(bookBorrow));
    }

    /**
     * 修改图书借阅
     */
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody BookBorrow bookBorrow) {
        return toAjax(bookBorrowService.updateBookBorrow(bookBorrow));
    }

    /**
     * 删除图书借阅
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(bookBorrowService.deleteBookBorrowByIds(ids));
    }

    /**
     * 我的借阅
     */
    @GetMapping("/myBorrowList")
    public TableDataInfo list(String borrowUser) {
        startPage();
        BookBorrow bookBorrowQuery = new BookBorrow();
        bookBorrowQuery.setBorrowUser(borrowUser);
        bookBorrowQuery.setBorrowStatus(BorrowStatusEnum.BORROW.getCode());
        List<BookBorrow> list = bookBorrowService.selectBookBorrowList(bookBorrowQuery);
        return getDataTable(list);
    }

    /**
     * 逾期未归还
     */
    @GetMapping("/overdueAndBorrowList")
    public TableDataInfo overdueAndBorrowList() {
        startPage();
        List<BookBorrow> list = bookBorrowService.overdueAndBorrowList();
        return getDataTable(list);
    }

    /**
     * 逾期归还
     */
    @GetMapping("/overdueAndBackList")
    public TableDataInfo overdueAndBackList() {
        startPage();
        List<BookBorrow> list = bookBorrowService.overdueAndBackList();
        return getDataTable(list);
    }

    /**
     * 手动，逾期提醒（钉钉）
     */
    @PostMapping("/noticeBook")
    public AjaxResult noticeBook(@RequestBody BookBorrow bookBorrow) {
        return bookBorrowService.noticeBook(bookBorrow);
    }
}
