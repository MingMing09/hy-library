package com.xgxx.hy.library.admin.competitionCode.controller;

import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.competitionCode.domain.AhpCaseA;
import com.xgxx.hy.library.admin.competitionCode.domain.AhpCaseB;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgRate;
import com.xgxx.hy.library.admin.competitionCode.domain.ProductionLine;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgRateService;
import com.xgxx.hy.library.admin.competitionCode.service.IProductionLineService;
import com.xgxx.hy.library.admin.common.core.controller.BaseController;
import com.xgxx.hy.library.admin.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 生产线Controller
 */
@RestController
@RequestMapping("/ahp/productionLine")
public class ProductionLineController extends BaseController {
    @Autowired
    private IProductionLineService productionLineService;
    @Autowired
    private IAlgRateService rateService;
    /**
     * 查询生产线列表11 111  2214
     */
    @GetMapping("/list")
    public TableDataInfo list(ProductionLine productionLine) {
        List<ProductionLine> list = productionLineService.selectBookList(productionLine);
        return getDataTable(list);
    }

    /**
     * AHP a
     *
     * @param ahpCases
     * @return
     */
    @PostMapping("/ahpCaseAs")
    public AjaxResult ahpCaseAs(@RequestBody List<AhpCaseA> ahpCases) {
        //入参最多9个。定死的。第一次入参是8个，再次调用，把最后一个去掉
        if(ahpCases.size()==9){
            ahpCases.remove(ahpCases.get(8));
        }
        //将表中数据放入map
        Map<String, BigDecimal> map = new HashMap();
        for (int i = 1; i < (ahpCases.size() + 1); i++) {
            for (int j = 1; j < 9; j++) {
                map.put(i + "-" + j, ahpCases.get(i - 1).getCase(j));
                if (j == i) {
                    map.put(i + "-" + j, BigDecimal.ONE);
                }
            }
        }
        //将map中 对角线上的数据进行 1/x 处理
        for (int i = 1; i < (ahpCases.size() + 1); i++) {
            for (int j = 1; j < 9; j++) {
                if (j < i) {
                    BigDecimal xx = (BigDecimal) map.get(j + "-" + i);
                    map.put(i + "-" + j, BigDecimal.ONE.divide(xx, BigDecimal.ROUND_HALF_UP, 2));
                }
            }
        }
        //再将 map数据转换成 集合
        List<AhpCaseA> ahpCaseList = new ArrayList<>();
        for (int i = 1; i < (ahpCases.size() + 1); i++) {
            AhpCaseA ahpCaseA = new AhpCaseA();
            for (int j = 1; j < 9; j++) {
                BigDecimal uu = (BigDecimal) map.get(i + "-" + j);
                ahpCaseA.setCase(j, uu);
            }
            ahpCaseList.add(ahpCaseA);
        }
        //求纵向之和(集合的最后一个对象，是求的和)
        AhpCaseA ahpCaseA = new AhpCaseA();
        for (int j = 1; j < 9; j++) {
            BigDecimal vv = BigDecimal.ZERO;
            for (int i = 1; i < (ahpCases.size() + 1); i++) {
                vv = vv.add(ahpCases.get(i - 1).getCase(j));
            }
            ahpCaseA.setCase(j, vv);
        }
        ahpCaseList.add(ahpCaseA);
        return AjaxResult.success(ahpCaseList);
    }

    /**
     * AHP a
     *
     * @param ahpCases
     * @return
     */
    @PostMapping("/ahpCaseBs")
    public AjaxResult ahpCaseBs(@RequestBody List<AhpCaseA> ahpCases) {
        List<AhpCaseB> ahpCaseBs = new ArrayList<>();
        //计算B 值：集合的最后一个对象，是求的和,所以要少循环一个
        for (int i = 1; i < (ahpCases.size()); i++) {
            AhpCaseB ahpCaseB = new AhpCaseB();
            for (int j = 1; j < 9; j++) {
                ahpCaseB.getCaseB(j, ahpCases.get(i - 1).getCase(j), ahpCases.get(ahpCases.size() - 1).getCase(j));
            }
            ahpCaseB.setSum();
            ahpCaseBs.add(ahpCaseB);
        }
        //求纵向之和(集合的最后一个对象，是求的和)
        AhpCaseB ahpCaseB = new AhpCaseB();//最后的一个求和对象
        for (int j = 1; j <= 9; j++) {
            BigDecimal vv = BigDecimal.ZERO;
            for (int i = 1; i < (ahpCaseBs.size() + 1); i++) {
                vv = vv.add(ahpCaseBs.get(i - 1).getCase(j));
            }
            ahpCaseB.setCase(j, vv);
        }
        ahpCaseBs.add(ahpCaseB);
        //计算权重集合的最后一个对象，是求的和,所以要少循环一个
        for (int i = 1; i < (ahpCaseBs.size()); i++) {
            ahpCaseBs.get(i - 1).setCase(10, ahpCaseBs.get(i - 1).getCaseSum().divide(ahpCaseBs.get(ahpCaseBs.size() - 1).getCase(9), BigDecimal.ROUND_HALF_UP, 2));
        }
        //将占比，根据 客户、物料，更新数据库
        rateService.updateRateByPramas(ahpCaseBs);
        return AjaxResult.success(ahpCaseBs);
    }

    @GetMapping("/moAhpCaseAs")
    public AjaxResult moAhpCaseAs() {
        List<AhpCaseA> ahpCaseList = new ArrayList<>();
        AhpCaseA ahpCaseA1=new AhpCaseA();
        ahpCaseA1.setCase1(new BigDecimal(1));
        ahpCaseA1.setCase2(new BigDecimal(2));
        ahpCaseA1.setCase3(new BigDecimal(3));
        ahpCaseA1.setCase4(new BigDecimal(2));
        ahpCaseA1.setCase5(new BigDecimal(5));
        ahpCaseA1.setCase6(new BigDecimal(0.5));
        ahpCaseA1.setCase7(new BigDecimal(1));
        ahpCaseA1.setCase8(new BigDecimal(1.5));

        AhpCaseA ahpCaseA2=new AhpCaseA();
        ahpCaseA2.setCase1(new BigDecimal(0.5));
        ahpCaseA2.setCase2(new BigDecimal(1));
        ahpCaseA2.setCase3(new BigDecimal(1.5));
        ahpCaseA2.setCase4(new BigDecimal(1));
        ahpCaseA2.setCase5(new BigDecimal(2.5));
        ahpCaseA2.setCase6(new BigDecimal(4));
        ahpCaseA2.setCase7(new BigDecimal(0.5));
        ahpCaseA2.setCase8(new BigDecimal(1.5));

        AhpCaseA ahpCaseA3=new AhpCaseA();
        ahpCaseA3.setCase1(new BigDecimal(0.33));
        ahpCaseA3.setCase2(new BigDecimal(0.66));
        ahpCaseA3.setCase3(new BigDecimal(1));
        ahpCaseA3.setCase4(new BigDecimal(0.66));
        ahpCaseA3.setCase5(new BigDecimal(2));
        ahpCaseA3.setCase6(new BigDecimal(5));
        ahpCaseA3.setCase7(new BigDecimal(0.66));
        ahpCaseA3.setCase8(new BigDecimal(0.33));

        AhpCaseA ahpCaseA4=new AhpCaseA();
        ahpCaseA4.setCase1(new BigDecimal(0.5));
        ahpCaseA4.setCase2(new BigDecimal(1));
        ahpCaseA4.setCase3(new BigDecimal(1.5));
        ahpCaseA4.setCase4(new BigDecimal(1));
        ahpCaseA4.setCase5(new BigDecimal(3));
        ahpCaseA4.setCase6(new BigDecimal(4));
        ahpCaseA4.setCase7(new BigDecimal(0.5));
        ahpCaseA4.setCase8(new BigDecimal(0.75));

        AhpCaseA ahpCaseA5=new AhpCaseA();
        ahpCaseA5.setCase1(new BigDecimal(0.2));
        ahpCaseA5.setCase2(new BigDecimal(0.4));
        ahpCaseA5.setCase3(new BigDecimal(0.5));
        ahpCaseA5.setCase4(new BigDecimal(0.33));
        ahpCaseA5.setCase5(new BigDecimal(1));
        ahpCaseA5.setCase6(new BigDecimal(5));
        ahpCaseA5.setCase7(new BigDecimal(0.2));
        ahpCaseA5.setCase8(new BigDecimal(0.3));

        AhpCaseA ahpCaseA6=new AhpCaseA();
        ahpCaseA6.setCase1(new BigDecimal(2));
        ahpCaseA6.setCase2(new BigDecimal(0.25));
        ahpCaseA6.setCase3(new BigDecimal(0.2));
        ahpCaseA6.setCase4(new BigDecimal(0.25));
        ahpCaseA6.setCase5(new BigDecimal(0.2));
        ahpCaseA6.setCase6(new BigDecimal(1));
        ahpCaseA6.setCase7(new BigDecimal(2));
        ahpCaseA6.setCase8(new BigDecimal(3));

        AhpCaseA ahpCaseA7=new AhpCaseA();
        ahpCaseA7.setCase1(new BigDecimal(1));
        ahpCaseA7.setCase2(new BigDecimal(2));
        ahpCaseA7.setCase3(new BigDecimal(1.5));
        ahpCaseA7.setCase4(new BigDecimal(2));
        ahpCaseA7.setCase5(new BigDecimal(5));
        ahpCaseA7.setCase6(new BigDecimal(0.5));
        ahpCaseA7.setCase7(new BigDecimal(1));
        ahpCaseA7.setCase8(new BigDecimal(1.5));

        AhpCaseA ahpCaseA8=new AhpCaseA();
        ahpCaseA8.setCase1(new BigDecimal(0.66));
        ahpCaseA8.setCase2(new BigDecimal(0.66));
        ahpCaseA8.setCase3(new BigDecimal(3));
        ahpCaseA8.setCase4(new BigDecimal(1.33));
        ahpCaseA8.setCase5(new BigDecimal(3.3));
        ahpCaseA8.setCase6(new BigDecimal(0.33));
        ahpCaseA8.setCase7(new BigDecimal(0.66));
        ahpCaseA8.setCase8(new BigDecimal(1));

        ahpCaseList.add(ahpCaseA1);
        ahpCaseList.add(ahpCaseA2);
        ahpCaseList.add(ahpCaseA3);
        ahpCaseList.add(ahpCaseA4);
        ahpCaseList.add(ahpCaseA5);
        ahpCaseList.add(ahpCaseA6);
        ahpCaseList.add(ahpCaseA7);
        ahpCaseList.add(ahpCaseA8);
        return AjaxResult.success(ahpCaseList);
    }
}
