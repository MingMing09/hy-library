package com.xgxx.hy.library.admin.library.enums;

public enum BookStatus {
    ZG(0, "在馆"),
    JC(1, "借出"),
    DS(2, "丢失");

    private Integer code; //编码
    private String desc; //描述

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static String getValue(Integer code) {
        for (BookStatus auditStatusCode : BookStatus.values()) {
            if (auditStatusCode.getCode().equals(code)) {
                return auditStatusCode.getDesc();
            }
        }

        return "";
    }

    BookStatus(int code, String desc) {
        this.code = (int)code;
        this.desc = desc;
    }
}