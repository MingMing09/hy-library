package com.xgxx.hy.library.admin.common.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 读取钉钉应用相关配置
 *
 * @author ruoyi
 */
@Component
@ConfigurationProperties(prefix = "dingtalk")
@Data
public class DingTalkConfig {
    /**
     * agentId
     */
    private String agentId;

    /**
     * appKey
     */
    private String appKey;

    /**
     * appSecret
     */
    private String appSecret;

}
