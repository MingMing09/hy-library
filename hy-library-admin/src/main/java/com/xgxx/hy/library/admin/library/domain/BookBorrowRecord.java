package com.xgxx.hy.library.admin.library.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xgxx.hy.library.admin.common.annotation.Excel;
import com.xgxx.hy.library.admin.common.core.domain.entity.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 图书借阅记录对象 book_borrow_record
 *
 * @author zyc
 * @date 2021-10-20
 */
@Data
public class BookBorrowRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 图书id
     */
    @Excel(name = "图书id")
    private String bookCode;

    /**
     * 借阅人
     */
    @Excel(name = "借阅人")
    private String borrowUser;

    /**
     * 记录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date recordTime;

    /**
     * 记录类型 0、借阅 1、续借 2、归还 3、挂失
     */
    @Excel(name = "记录类型 0、借阅 1、续借 2、归还 3、挂失 4、挂失归还")
    private Integer recordType;

    @TableField(exist = false)
    private Book book;

    @TableField(exist = false)
    private SysUser sysUser;
}
