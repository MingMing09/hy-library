package com.xgxx.hy.library.admin.competitionCode.service.impl;

import java.util.List;

import com.xgxx.hy.library.admin.competitionCode.domain.AhpCaseB;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgRate;
import com.xgxx.hy.library.admin.competitionCode.mapper.AlgRateMapper;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 实时占比Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-20
 */
@Service
public class AlgRateServiceImpl implements IAlgRateService {
    @Autowired
    private AlgRateMapper algRateMapper;

    /**
     * 查询实时占比
     *
     * @param id 实时占比主键
     * @return 实时占比
     */
    @Override
    public AlgRate selectAlgRateById(Long id) {
        return algRateMapper.selectAlgRateById(id);
    }

    /**
     * 查询实时占比列表
     *
     * @param algRate 实时占比
     * @return 实时占比
     */
    @Override
    public List<AlgRate> selectAlgRateList(AlgRate algRate) {
        return algRateMapper.selectAlgRateList(algRate);
    }

    /**
     * 新增实时占比
     *
     * @param algRate 实时占比
     * @return 结果
     */
    @Override
    public int insertAlgRate(AlgRate algRate) {
        return algRateMapper.insertAlgRate(algRate);
    }

    /**
     * 修改实时占比
     *
     * @param algRate 实时占比
     * @return 结果
     */
    @Override
    public int updateAlgRate(AlgRate algRate) {
        return algRateMapper.updateAlgRate(algRate);
    }

    /**
     * 批量删除实时占比
     *
     * @param ids 需要删除的实时占比主键
     * @return 结果
     */
    @Override
    public int deleteAlgRateByIds(Long[] ids) {
        return algRateMapper.deleteAlgRateByIds(ids);
    }

    /**
     * 删除实时占比信息
     *
     * @param id 实时占比主键
     * @return 结果
     */
    @Override
    public int deleteAlgRateById(Long id) {
        return algRateMapper.deleteAlgRateById(id);
    }

    @Override
    public int updateRateByPramas(List<AhpCaseB> ahpCaseBs) {
        for (int j = 1; j < (ahpCaseBs.size()); j++) {
            AhpCaseB ahpCaseB = ahpCaseBs.get(j - 1);
            if (j == 1 && null != ahpCaseB) {
                AlgRate algRate = new AlgRate();
                algRate.setCustomerCode("CUSTOMER-A");
                algRate.setMaterialCode("MATERIAL-A");
                List<AlgRate> algRates = this.selectAlgRateList(algRate);
                if (algRates.isEmpty()) {
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.insertAlgRate(algRate);
                } else {
                    algRate = algRates.get(0);
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.updateAlgRate(algRate);
                }
            } else if (j == 2 && null != ahpCaseB) {
                AlgRate algRate = new AlgRate();
                algRate.setCustomerCode("CUSTOMER-B");
                algRate.setMaterialCode("MATERIAL-A");
                List<AlgRate> algRates = this.selectAlgRateList(algRate);
                if (algRates.isEmpty()) {
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.insertAlgRate(algRate);
                } else {
                    algRate = algRates.get(0);
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.updateAlgRate(algRate);
                }
            } else if (j == 3 && null != ahpCaseB) {
                AlgRate algRate = new AlgRate();
                algRate.setCustomerCode("CUSTOMER-C");
                algRate.setMaterialCode("MATERIAL-A");
                List<AlgRate> algRates = this.selectAlgRateList(algRate);
                if (algRates.isEmpty()) {
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.insertAlgRate(algRate);
                } else {
                    algRate = algRates.get(0);
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.updateAlgRate(algRate);
                }
            } else if (j == 4 && null != ahpCaseB) {
                AlgRate algRate = new AlgRate();
                algRate.setCustomerCode("CUSTOMER-A");
                algRate.setMaterialCode("MATERIAL-B");
                List<AlgRate> algRates = this.selectAlgRateList(algRate);
                if (algRates.isEmpty()) {
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.insertAlgRate(algRate);
                } else {
                    algRate = algRates.get(0);
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.updateAlgRate(algRate);
                }
            } else if (j == 5 && null != ahpCaseB) {
                AlgRate algRate = new AlgRate();
                algRate.setCustomerCode("CUSTOMER-C");
                algRate.setMaterialCode("MATERIAL-B");
                List<AlgRate> algRates = this.selectAlgRateList(algRate);
                if (algRates.isEmpty()) {
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.insertAlgRate(algRate);
                } else {
                    algRate = algRates.get(0);
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.updateAlgRate(algRate);
                }
            } else if (j == 6 && null != ahpCaseB) {
                AlgRate algRate = new AlgRate();
                algRate.setCustomerCode("CUSTOMER-D");
                algRate.setMaterialCode("MATERIAL-A");
                List<AlgRate> algRates = this.selectAlgRateList(algRate);
                if (algRates.isEmpty()) {
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.insertAlgRate(algRate);
                } else {
                    algRate = algRates.get(0);
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.updateAlgRate(algRate);
                }
            } else if (j == 7 && null != ahpCaseB) {
                AlgRate algRate = new AlgRate();
                algRate.setCustomerCode("CUSTOMER-D");
                algRate.setMaterialCode("MATERIAL-B");
                List<AlgRate> algRates = this.selectAlgRateList(algRate);
                if (algRates.isEmpty()) {
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.insertAlgRate(algRate);
                } else {
                    algRate = algRates.get(0);
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.updateAlgRate(algRate);
                }
            } else if (j == 8 && null != ahpCaseB) {
                AlgRate algRate = new AlgRate();
                algRate.setCustomerCode("CUSTOMER-D");
                algRate.setMaterialCode("MATERIAL-C");
                List<AlgRate> algRates = this.selectAlgRateList(algRate);
                if (algRates.isEmpty()) {
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.insertAlgRate(algRate);
                } else {
                    algRate = algRates.get(0);
                    algRate.setRate(ahpCaseB.getCaseRate());
                    this.updateAlgRate(algRate);
                }
            }
        }
        return 0;
    }
}
