package com.xgxx.hy.library.admin.library.mapper;

import java.util.List;
import com.xgxx.hy.library.admin.library.domain.BookBorrowRecord;

/**
 * 图书借阅记录Mapper接口
 * 
 * @author zyc
 * @date 2021-10-20
 */
public interface BookBorrowRecordMapper 
{
    /**
     * 查询图书借阅记录
     * 
     * @param id 图书借阅记录主键
     * @return 图书借阅记录
     */
    public BookBorrowRecord selectBookBorrowRecordById(Long id);

    /**
     * 查询图书借阅记录列表
     * 
     * @param bookBorrowRecord 图书借阅记录
     * @return 图书借阅记录集合
     */
    public List<BookBorrowRecord> selectBookBorrowRecordList(BookBorrowRecord bookBorrowRecord);

    /**
     * 新增图书借阅记录
     * 
     * @param bookBorrowRecord 图书借阅记录
     * @return 结果
     */
    public int insertBookBorrowRecord(BookBorrowRecord bookBorrowRecord);

    /**
     * 修改图书借阅记录
     * 
     * @param bookBorrowRecord 图书借阅记录
     * @return 结果
     */
    public int updateBookBorrowRecord(BookBorrowRecord bookBorrowRecord);

    /**
     * 删除图书借阅记录
     * 
     * @param id 图书借阅记录主键
     * @return 结果
     */
    public int deleteBookBorrowRecordById(Long id);

    /**
     * 批量删除图书借阅记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBookBorrowRecordByIds(Long[] ids);
}
