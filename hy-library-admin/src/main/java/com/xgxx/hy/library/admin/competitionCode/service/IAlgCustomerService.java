package com.xgxx.hy.library.admin.competitionCode.service;

import com.xgxx.hy.library.admin.competitionCode.domain.AlgCustomer;

import java.util.List;

/**
 * 客户Service接口
 * 
 * @author ruoyi
 * @date 2022-10-18
 */
public interface IAlgCustomerService 
{
    /**
     * 查询客户
     * 
     * @param id 客户主键
     * @return 客户
     */
    public AlgCustomer selectAlgCustomerById(Long id);

    /**
     * 查询客户列表
     * 
     * @param algCustomer 客户
     * @return 客户集合
     */
    public List<AlgCustomer> selectAlgCustomerList(AlgCustomer algCustomer);

    /**
     * 新增客户
     * 
     * @param algCustomer 客户
     * @return 结果
     */
    public int insertAlgCustomer(AlgCustomer algCustomer);

    /**
     * 修改客户
     * 
     * @param algCustomer 客户
     * @return 结果
     */
    public int updateAlgCustomer(AlgCustomer algCustomer);

    /**
     * 批量删除客户
     * 
     * @param ids 需要删除的客户主键集合
     * @return 结果
     */
    public int deleteAlgCustomerByIds(Long[] ids);

    /**
     * 删除客户信息
     * 
     * @param id 客户主键
     * @return 结果
     */
    public int deleteAlgCustomerById(Long id);
}
