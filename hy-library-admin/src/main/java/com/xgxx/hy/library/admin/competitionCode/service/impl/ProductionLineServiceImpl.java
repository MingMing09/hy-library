package com.xgxx.hy.library.admin.competitionCode.service.impl;

import com.xgxx.hy.library.admin.competitionCode.domain.ProductionLine;
import com.xgxx.hy.library.admin.competitionCode.mapper.ProductionLineMapper;
import com.xgxx.hy.library.admin.competitionCode.service.IProductionLineService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 生产线Service业务层处理
 *
 * @author mYm
 */
@Service
@AllArgsConstructor
public class ProductionLineServiceImpl implements IProductionLineService {
    private ProductionLineMapper bookMapper;

    @Override
    public List<ProductionLine> selectBookList(ProductionLine productionLine) {
        return bookMapper.selectProductionLineList(productionLine);
    }
}
