package com.xgxx.hy.library.admin.competitionCode.domain;

import com.xgxx.hy.library.admin.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 生产线对象 book
 *
 */
@Data
public class ProductionLine implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    @Excel(name = "名称")
    private String name;

    /**
     * 国际标准图书编号
     */
    @Excel(name = "生产线时间")
    private BigDecimal time;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private Integer lineStatus;

}
