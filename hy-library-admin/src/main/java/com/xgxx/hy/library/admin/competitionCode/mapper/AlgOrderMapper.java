package com.xgxx.hy.library.admin.competitionCode.mapper;

import java.util.List;

import com.xgxx.hy.library.admin.competitionCode.domain.AlgOrder;

/**
 * 订单Mapper接口
 * 
 * @author ruoyi
 * @date 2022-10-18
 */
public interface AlgOrderMapper 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public AlgOrder selectAlgOrderById(Long id);

    /**
     * 查询订单列表
     * 
     * @param algOrder 订单
     * @return 订单集合
     */
    public List<AlgOrder> selectAlgOrderList(AlgOrder algOrder);

    /**
     * 新增订单
     * 
     * @param algOrder 订单
     * @return 结果
     */
    public int insertAlgOrder(AlgOrder algOrder);

    /**
     * 修改订单
     * 
     * @param algOrder 订单
     * @return 结果
     */
    public int updateAlgOrder(AlgOrder algOrder);

    /**
     * 删除订单
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteAlgOrderById(Long id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAlgOrderByIds(Long[] ids);
}
