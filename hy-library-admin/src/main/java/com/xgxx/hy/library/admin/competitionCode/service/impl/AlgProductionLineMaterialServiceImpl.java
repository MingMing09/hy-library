package com.xgxx.hy.library.admin.competitionCode.service.impl;

import java.util.List;

import com.xgxx.hy.library.admin.competitionCode.domain.AlgProductionLineMaterial;
import com.xgxx.hy.library.admin.competitionCode.mapper.AlgProductionLineMaterialMapper;
import com.xgxx.hy.library.admin.competitionCode.service.IAlgProductionLineMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 产线物料配置Service业务层处理
 *
 * @author ruoyi
 * @date 2022-10-18
 */
@Service
public class AlgProductionLineMaterialServiceImpl implements IAlgProductionLineMaterialService
{
    @Autowired
    private AlgProductionLineMaterialMapper algProductionLineMaterialMapper;

    /**
     * 查询产线物料配置
     *
     * @param id 产线物料配置主键
     * @return 产线物料配置
     */
    @Override
    public AlgProductionLineMaterial selectAlgProductionLineMaterialById(Long id)
    {
        return algProductionLineMaterialMapper.selectAlgProductionLineMaterialById(id);
    }

    /**
     * 查询产线物料配置列表
     *
     * @param algProductionLineMaterial 产线物料配置
     * @return 产线物料配置
     */
    @Override
    public List<AlgProductionLineMaterial> selectAlgProductionLineMaterialList(AlgProductionLineMaterial algProductionLineMaterial)
    {
        return algProductionLineMaterialMapper.selectAlgProductionLineMaterialList(algProductionLineMaterial);
    }

    /**
     * 新增产线物料配置
     *
     * @param algProductionLineMaterial 产线物料配置
     * @return 结果
     */
    @Override
    public int insertAlgProductionLineMaterial(AlgProductionLineMaterial algProductionLineMaterial)
    {
        return algProductionLineMaterialMapper.insertAlgProductionLineMaterial(algProductionLineMaterial);
    }

    /**
     * 修改产线物料配置
     *
     * @param algProductionLineMaterial 产线物料配置
     * @return 结果
     */
    @Override
    public int updateAlgProductionLineMaterial(AlgProductionLineMaterial algProductionLineMaterial)
    {
        return algProductionLineMaterialMapper.updateAlgProductionLineMaterial(algProductionLineMaterial);
    }

    /**
     * 批量删除产线物料配置
     *
     * @param ids 需要删除的产线物料配置主键
     * @return 结果
     */
    @Override
    public int deleteAlgProductionLineMaterialByIds(Long[] ids)
    {
        return algProductionLineMaterialMapper.deleteAlgProductionLineMaterialByIds(ids);
    }

    /**
     * 删除产线物料配置信息
     *
     * @param id 产线物料配置主键
     * @return 结果
     */
    @Override
    public int deleteAlgProductionLineMaterialById(Long id)
    {
        return algProductionLineMaterialMapper.deleteAlgProductionLineMaterialById(id);
    }
}
