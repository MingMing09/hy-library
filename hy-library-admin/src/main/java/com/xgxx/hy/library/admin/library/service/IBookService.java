package com.xgxx.hy.library.admin.library.service;

import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.common.core.domain.entity.SysUser;
import com.xgxx.hy.library.admin.library.domain.Book;

import java.util.List;

/**
 * 图书Service接口
 *
 * @author zyc
 * @date 2021-10-20
 */
public interface IBookService {
    /**
     * 查询图书
     *
     * @param id 图书主键
     * @return 图书
     */
    public Book selectBookById(Long id);

    /**
     * 查询图书列表
     *
     * @param book 图书
     * @return 图书集合
     */
    public List<Book> selectBookList(Book book);

    /**
     * 新增图书
     *
     * @param book 图书
     * @return 结果
     */
    public AjaxResult insertBook(Book book);

    /**
     * 修改图书
     *
     * @param book 图书
     * @return 结果
     */
    public AjaxResult updateBook(Book book);

    /**
     * 批量删除图书
     *
     * @param ids 需要删除的图书主键集合
     * @return 结果
     */
    public int deleteBookByIds(Long[] ids);

    /**
     * 删除图书信息
     *
     * @param id 图书主键
     * @return 结果
     */
    public int deleteBookById(Long id);

    /**
     * 扫码，入库
     *
     * @param dataStr
     */
    AjaxResult splitDataToInsert(String dataStr);

    /**
     * 图书借阅（借阅入口）
     *
     * @param book
     * @return
     */
    AjaxResult bookToBorrow(Book book);
    /**
     * 图书续借（续借入口）
     *
     * @param book
     * @return
     */
    AjaxResult bookAgainBorrow(Book book);
    /**
     * 图书归还（归还入口）
     *
     * @param book
     * @return
     */
    AjaxResult bookBackBorrow(Book book);
    /**
     * 图书挂失（挂失入口）
     *
     * @param book
     * @return
     */
    AjaxResult bookLoseBorrow(Book book);

    /**
     * 图书挂失（挂失入口，不关联记录）
     *
     * @param book
     * @return
     */
    AjaxResult bookLoseBorrowPc(Book book);

    /**
     * 根据图书编码获取图书信息
     * @param bookCode
     * @return
     */
    Book selectBookByCode(String bookCode);
    /**
     * 根据号码，获取详情信息
     * @param user
     * @return
     */
    public AjaxResult getUserByPhone(SysUser user);

    /**
     * 前十排行榜
     * @return
     */
    public AjaxResult getUserPhang();

    SysUser getUserInfo(String borrowUser);
}
