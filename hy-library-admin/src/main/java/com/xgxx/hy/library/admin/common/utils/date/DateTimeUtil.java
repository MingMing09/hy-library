package com.xgxx.hy.library.admin.common.utils.date;

import org.apache.commons.lang.StringUtils;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author liwt
 * @version V2.4.0
 * @date 2021年10月11日 11:51
 * @Copyright:2021 徐工信息 All rights reserved.
 */
public class DateTimeUtil {

    public static final String ZERO_TIME_DEFAULT = " 00:00:00";
    public static final String MAX_TIME_DEFAULT = " 23:59:59";
    public static final String PATTERN_YMD = "yyyy年MM月dd日";
    public static final DateTimeFormatter DATETIME_FORMAT = DateTimeFormatter.ofPattern(DateUtil.PATTERN_DATETIME);
    public static final DateTimeFormatter DATETIME_FORMAT_H = DateTimeFormatter.ofPattern(DateUtil.PATTERN_DATETIME_H);
    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern(DateUtil.PATTERN_DATE);
    public static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern(DateUtil.PATTERN_TIME);

    /**
     * 获取日期最小时间
     *
     * @param date
     * @return
     */
    public static Date getZeroDate(Date date) {
        String zeroTime = DateUtil.format(date, DATE_FORMAT) + ZERO_TIME_DEFAULT;
        return DateUtil.parse(zeroTime, DATETIME_FORMAT);
    }

    /**
     * 获取日期最大时间
     *
     * @param date
     * @return
     */
    public static Date getMaxDate(Date date) {
        String maxTime = DateUtil.format(date, DATE_FORMAT) + MAX_TIME_DEFAULT;
        return DateUtil.parse(maxTime, DATETIME_FORMAT);
    }

    /**
     * 获取分钟单位
     *
     * @param lastTime 1m 1h 1d
     * @return
     */
    public static int getMinute(String lastTime) {
        if (StringUtils.isEmpty(lastTime)) {
            return 0;
        }

        String data = lastTime.substring(0, lastTime.length() - 2);
        int minutes = 0;
        if (lastTime.endsWith("m")) {
            minutes = Integer.valueOf(data);
        }
        if (lastTime.endsWith("h")) {
            minutes = Integer.valueOf(data) * 60;
        }
        if (lastTime.endsWith("d")) {
            minutes = Integer.valueOf(data) * 24 * 60;
        }
        return minutes;
    }

    /**
     * 获取距离当前时间最近xxx
     *
     * @param lastTime
     * @return
     */
    public static Date getBeforeDate(int lastTime) {
        return DateUtil.offsetMinute(DateUtil.date(), -lastTime);
    }

    /**
     * @param startTime yyyy-MM-dd
     * @param endTime   yyyy-MM-dd
     * @return
     */
    public static List<String> getDaysBetween(Date startTime, Date endTime) {
        // 返回的日期集合
        List<String> days = new ArrayList<>();

        Date start = DateUtil.parse(DateUtil.format(startTime, DateUtil.PATTERN_DATE), DateUtil.PATTERN_DATE);
        Date end = DateUtil.parse(DateUtil.format(endTime, DateUtil.PATTERN_DATE), DateUtil.PATTERN_DATE);

        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(start);
        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(end);
        tempEnd.add(Calendar.DATE, +1);// 日期加1(包含结束)

        while (tempStart.before(tempEnd)) {
            days.add(DateUtil.format(tempStart.getTime(), PATTERN_YMD));
            tempStart.add(Calendar.DAY_OF_YEAR, 1);
        }
        return days;
    }

    /**
     * @param startTime yyyy-MM-dd HH:mm
     * @param endTime   yyyy-MM-dd HH:mm
     * @return
     */
    public static List<String> getHoursBetween(Date startTime, Date endTime) {
        // 返回的日期集合
        List<String> days = new ArrayList<>();

        Date start = DateUtil.parse(DateUtil.format(startTime, DATETIME_FORMAT_H), DATETIME_FORMAT_H);
        Date end = DateUtil.parse(DateUtil.format(endTime, DATETIME_FORMAT_H), DATETIME_FORMAT_H);

        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(start);
        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(end);
        tempEnd.add(Calendar.HOUR_OF_DAY, +1);

        while (tempStart.before(tempEnd)) {
            days.add(DateUtil.format(tempStart.getTime(), DateUtil.PATTERN_DATETIME_H));
            tempStart.add(Calendar.HOUR_OF_DAY, 1);
        }
        return days;
    }

    /**
     * @param startTime yyyy-MM-dd HH:mm:ss
     * @param endTime   yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static List<String> getMinutesBetween(Date startTime, Date endTime) {

        Date start = DateUtil.parse(DateUtil.format(startTime, DATETIME_FORMAT), DATETIME_FORMAT);
        Date end = DateUtil.parse(DateUtil.format(endTime, DATETIME_FORMAT), DATETIME_FORMAT);

        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(end);
        tempEnd.add(Calendar.MINUTE, +1);
        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(start);

        // 返回的日期集合
        List<String> days = new ArrayList<>();
        while (tempStart.before(tempEnd)) {
            days.add(DateUtil.format(tempStart.getTime(), DateUtil.PATTERN_DATETIME));
            tempStart.add(Calendar.MINUTE, 1);
        }
        return days;
    }

}
