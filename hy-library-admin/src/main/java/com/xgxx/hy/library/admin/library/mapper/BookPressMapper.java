package com.xgxx.hy.library.admin.library.mapper;

import java.util.List;
import com.xgxx.hy.library.admin.library.domain.BookPress;

/**
 * 出版社Mapper接口
 * 
 * @author zyc
 * @date 2021-10-20
 */
public interface BookPressMapper 
{
    /**
     * 查询出版社
     * 
     * @param id 出版社主键
     * @return 出版社
     */
    public BookPress selectBookPressById(Long id);

    /**
     * 查询出版社列表
     * 
     * @param bookPress 出版社
     * @return 出版社集合
     */
    public List<BookPress> selectBookPressList(BookPress bookPress);

    public List<BookPress> selectBookPressNameList(BookPress bookPress);

    /**
     * 新增出版社
     * 
     * @param bookPress 出版社
     * @return 结果
     */
    public int insertBookPress(BookPress bookPress);

    /**
     * 修改出版社
     * 
     * @param bookPress 出版社
     * @return 结果
     */
    public int updateBookPress(BookPress bookPress);

    /**
     * 删除出版社
     * 
     * @param id 出版社主键
     * @return 结果
     */
    public int deleteBookPressById(Long id);

    /**
     * 批量删除出版社
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBookPressByIds(Long[] ids);
}
