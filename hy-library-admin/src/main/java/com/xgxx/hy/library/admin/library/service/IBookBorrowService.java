package com.xgxx.hy.library.admin.library.service;

import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.library.domain.BookBorrow;

import java.util.Date;
import java.util.List;

/**
 * 图书借阅Service接口
 *
 * @author zyc
 * @date 2021-10-20
 */
public interface IBookBorrowService {
    /**
     * 查询图书借阅
     *
     * @param id 图书借阅主键
     * @return 图书借阅
     */
    public BookBorrow selectBookBorrowById(Long id);

    /**
     * 查询图书借阅
     *
     * @param bookBorrow
     * @return 图书借阅对象
     */
    public BookBorrow selectBookBorrowByUser(BookBorrow bookBorrow);

    /**
     * 查询图书借阅列表
     *
     * @param bookBorrow 图书借阅
     * @return 图书借阅集合
     */
    public List<BookBorrow> selectBookBorrowList(BookBorrow bookBorrow);

    /**
     * 新增图书借阅
     *
     * @param bookBorrow bookCode borrowUser 必填
     * @return 结果
     */
    public int insertBookBorrow(BookBorrow bookBorrow);

    /**
     * 修改图书借阅
     *
     * @param bookBorrow 图书借阅
     * @return 结果
     */
    public int updateBookBorrow(BookBorrow bookBorrow);

    /**
     * 批量删除图书借阅
     *
     * @param ids 需要删除的图书借阅主键集合
     * @return 结果
     */
    public int deleteBookBorrowByIds(Long[] ids);

    /**
     * 删除图书借阅信息
     *
     * @param id 图书借阅主键
     * @return 结果
     */
    public int deleteBookBorrowById(Long id);

    /**
     * 借阅
     *
     * @param bookBorrow 借阅
     * @return 结果
     */
    public AjaxResult bookBorrow(BookBorrow bookBorrow);

    /**
     * 续借
     *
     * @param bookBorrow 续借
     * @return 结果
     */
    public AjaxResult bookBorrowAgain(BookBorrow bookBorrow);

    /**
     * 归还
     *
     * @param bookBorrow 归还
     * @return 结果
     */
    public AjaxResult bookBorrowBack(BookBorrow bookBorrow);

    /**
     * 丢失
     *
     * @param bookBorrow 丢失
     * @return 结果
     */
    public AjaxResult bookMiss(BookBorrow bookBorrow);

    /**
     * 丢失归还
     *
     * @param bookBorrow 丢失归还
     * @return 结果
     */
    public AjaxResult bookMissBack(BookBorrow bookBorrow);

    /**
     * 获取逾期未还列表
     *
     * @return
     */
    public List<BookBorrow> overdueAndBorrowList();

    /**
     * 获取逾期归还列表
     *
     * @return
     */
    public List<BookBorrow> overdueAndBackList();

    /**
     * 是否逾期
     * 会更新借阅对象借阅时间
     *
     * @param bookBorrow 借阅对象
     * @param now        当前时间
     * @return boolean
     */
    public boolean isOverdue(BookBorrow bookBorrow, Date now);

    /**
     * 每天0点执行，30天逾期，提前5天逾期提醒，钉钉通知
     *
     * @return
     */
    public AjaxResult overdueAndBorrowAndBoticeList();

    /**
     * 手动，逾期提醒（钉钉）
     *
     * @param bookBorrow
     * @return
     */
    public AjaxResult noticeBook(BookBorrow bookBorrow);
}
