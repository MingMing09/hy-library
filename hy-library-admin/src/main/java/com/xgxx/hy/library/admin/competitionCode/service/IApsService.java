package com.xgxx.hy.library.admin.competitionCode.service;

import com.xgxx.hy.library.admin.common.core.domain.AjaxResult;
import com.xgxx.hy.library.admin.competitionCode.domain.AlgOrder;

import java.util.Date;
import java.util.List;

/**
 * @author zyc
 * @date 2022/10/18
 **/
public interface IApsService {
    AjaxResult doAps(List<AlgOrder> orderList, Date startTime, Date endTime);
}
