package com.xgxx.hy.library.admin.library.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xgxx.hy.library.admin.common.core.domain.entity.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 图书借阅对象 book_borrow
 *
 * @author zyc
 * @date 2021-10-20
 */
@Data
public class BookBorrow implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 图书id
     */
    private String bookCode;

    /**
     * 借阅人
     */
    private String borrowUser;

    /**
     * 借阅开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date borrowStartTime;

    /**
     * 是否续借 0、否 1、是
     */
    private Integer borrowAgain;

    /**
     * 续借时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date borrowAgainTime;

    /**
     * 借阅状态 0、在借 1、归还 2、丢失
     */
    private Integer borrowStatus;

    /**
     * 是否逾期 0、否 1、是
     */
    private Integer borrowIsOverdue;

    @TableField(exist = false)
    private Long borrowDays;

    @TableField(exist = false)
    private String borrowStatusStr;

    /**
     * 借阅结束时间（归还、丢失）
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date borrowFinishTime;

    @TableField(exist = false)
    private Book book;

    @TableField(exist = false)
    private SysUser sysUser;
}
