package com.xgxx.hy.library.admin.common.utils;

import org.springframework.lang.Nullable;

/**
 * @author liwt
 * @version V2.4.0
 * @date 2021年10月11日 11:15
 * @Copyright:2021 徐工信息 All rights reserved.
 */
public class NumberUtil extends org.springframework.util.NumberUtils {

    public static long toLong(@Nullable final String str, final long defaultValue) {
        if (str == null) {
            return defaultValue;
        }
        try {
            return Long.valueOf(str);
        } catch (final NumberFormatException nfe) {
            return defaultValue;
        }
    }
}
